﻿// Xamarin Studio on MAcOSX 10.9
// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using System.IO.Compression;

namespace getIDfsdf2sdf
{
	class MainClass
	{
		public static void Main (string [] args)
		{

			Console.WriteLine ("start sdf to sdf");
			StringBuilder sb = new StringBuilder ();
			StringBuilder sblog = new StringBuilder ();
			string file = String.Empty;
			string [] delimiter = { " " };

			for (int N = 0; N < Environment.GetCommandLineArgs ().Length; N++) {

				file = Environment.GetCommandLineArgs () [N];

				if (file.Equals ("-h") || file.Equals ("-h")) {
					Console.WriteLine ("RCSB-PDB sdf to sdf: add id");
					Console.WriteLine ("Version JUn 10 2016. rev. A");
					return;
				}

				file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/20160610_Components-pub.sdf";

				int inputCount = 1;
				int lineCount = 1;
				Boolean bl_newdata = false;
				List<string> componentlines = new List<string> ();
				string componentline = String.Empty;
				int newdatacount = 1;

				if (File.Exists (file)) {


					if (file.ToLower ().EndsWith (".sdf")) {

						using (StreamReader reader = new StreamReader (file)) {
							while ((componentline = reader.ReadLine ()) != null) {



								if (reader.Peek () == -1) {
									bl_newdata = true;
								}

								if (lineCount > 1 && componentline.Length > 3 && componentline.Equals ("$$$$")) {
									bl_newdata = true;
									inputCount++;
								} else {
									if (!componentline.Equals ("$$$$")) {
										componentlines.Add (componentline);
										sb.AppendLine (componentline);
									}
									lineCount++;
								}

								if (bl_newdata == true) {

									string strId = string.Empty;

									string moldata = String.Empty;

									for (int c = 0; c < componentlines.Count; c++) {
										moldata += componentlines [c] + System.Environment.NewLine;

										if (c == 0) {
											strId = componentlines [0];
										}
									}

									Console.WriteLine (newdatacount + "\t" + strId);
									newdatacount++;


									sb.AppendLine ("> <PDB_CC_ID>");
									sb.AppendLine (strId);
									sb.AppendLine ();
									sb.AppendLine ("$$$$");


									bl_newdata = false;
									componentlines = new List<string> ();
									//componentlines.Add (componentline);

								}


							}
						}


						DateTime dt = DateTime.Now;
						string datetimesting = dt.ToString ("MM-dd-yyyy-HH-mm");

						StreamWriter writer = new StreamWriter (file + "_RCSB-PDB_sdf2sdf.add-id." + datetimesting + ".sdf");

						writer.Write (sb.ToString ());
						writer.Close ();


					}
				}
			}
		}
	}
}
