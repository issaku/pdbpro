﻿// Xamarin Studio on MAcOSX 10.10
// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections;
using System.IO.Compression;

namespace GlycoNAVI
{
	public class ConnectUtility
	{
		// struct_conn.id
		// PDB connect information
		public void Get_mmcif_connect (List<string> mmcifStrings, ref int connectCount, ref List<Bond> mmcifBonds)
		{
			#region 

			/*
			loop_
			_struct_conn.id 
			_struct_conn.conn_type_id 
			_struct_conn.pdbx_PDB_id 
			_struct_conn.ptnr1_label_asym_id 
			_struct_conn.ptnr1_label_comp_id 
			_struct_conn.ptnr1_label_seq_id 
			_struct_conn.ptnr1_label_atom_id 
			_struct_conn.pdbx_ptnr1_label_alt_id 
			_struct_conn.pdbx_ptnr1_PDB_ins_code 
			_struct_conn.pdbx_ptnr1_standard_comp_id 
			_struct_conn.ptnr1_symmetry 
			_struct_conn.ptnr2_label_asym_id 
			_struct_conn.ptnr2_label_comp_id 
			_struct_conn.ptnr2_label_seq_id 
			_struct_conn.ptnr2_label_atom_id 
			_struct_conn.pdbx_ptnr2_label_alt_id 
			_struct_conn.pdbx_ptnr2_PDB_ins_code 
			_struct_conn.ptnr1_auth_asym_id 
			_struct_conn.ptnr1_auth_comp_id 
			_struct_conn.ptnr1_auth_seq_id 
			_struct_conn.ptnr2_auth_asym_id 
			_struct_conn.ptnr2_auth_comp_id 
			_struct_conn.ptnr2_auth_seq_id 
			_struct_conn.ptnr2_symmetry 
			_struct_conn.pdbx_ptnr3_label_atom_id 
			_struct_conn.pdbx_ptnr3_label_seq_id 
			_struct_conn.pdbx_ptnr3_label_comp_id 
			_struct_conn.pdbx_ptnr3_label_asym_id 
			_struct_conn.pdbx_ptnr3_label_alt_id 
			_struct_conn.pdbx_ptnr3_PDB_ins_code 
			_struct_conn.details 
			_struct_conn.pdbx_dist_value 
			_struct_conn.pdbx_value_order 
			disulf1  disulf ? A CYS 209 SG  ? ? ? 1_555 A CYS 212 SG ? ? A CYS 210 A CYS 213 1_555 ? ? ? ? ? ? ? 2.029 ? 
			disulf2  disulf ? A CYS 221 SG  ? ? ? 1_555 A CYS 448 SG ? ? A CYS 222 A CYS 449 1_555 ? ? ? ? ? ? ? 2.021 ? 
			disulf3  disulf ? A CYS 261 SG  ? ? ? 1_555 A CYS 269 SG ? ? A CYS 262 A CYS 270 1_555 ? ? ? ? ? ? ? 2.035 ? 
			covale1  covale ? A ASN 170 ND2 ? ? ? 1_555 L NAG .   C1 ? ? A ASN 171 A NAG 483 1_555 ? ? ? ? ? ? ? 1.455 ? 
			*/

			#endregion

			for (int i = 0; i < mmcifStrings.Count; i++) {
				//foreach (var line in mmcifString) {
				//行毎に　”SUGAR”を確認
				//if (mmcifStrings [i].Contains ("GLYCOSYLATION SITE")) {
				//}
			}


		}


	}
}

