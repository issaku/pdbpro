﻿// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections;

namespace GlycoNAVI
{
	public class Chemical
	{
		// _struct_conn.id
		// PDB connect information




		// sp, sp2, sp3 of atom
		// (ref local_error_sb, ref CheckBondAtoms, ref bonds, pdb4code);
		public static void checkCOCNbond(ref StringBuilder log_sb, ref StringBuilder local_error_sb, ref List<Atom> a_atomdata, ref List<Bond> a_bonds, string a_idString){
			// check angle about C-O or C=O, C#O bonds.
			//public stanetic double AngleOf2Atoms(Atom A0, Atom A1, Atom B0, Atom B1 ){
			List<string> hetero_elements = new List<string> { "O", "N"};

			for (int j = 0; j < a_bonds.Count; j++) {
				try {
					if (a_atomdata[a_bonds [j].fromAtomID - 1].atom_site_type_symbol == "C" && hetero_elements.Contains(a_atomdata[a_bonds [j].toAtomID - 1].atom_site_type_symbol)) {
						List<Atom> obj_atms = GetConnectAtoms (ref log_sb, a_atomdata [a_bonds [j].fromAtomID - 1], a_atomdata, a_bonds);

						if (obj_atms.Count == 4) {
							a_bonds [j].BondOrder = 1;
						}
						else if (obj_atms.Count == 3) {

							// check angles

							double o_angle1 = AngleOf2Atoms(a_atomdata[a_bonds [j].fromAtomID - 1 ], obj_atms[0], a_atomdata[a_bonds [j].fromAtomID - 1 ], obj_atms[1] );
							double o_angle2 = AngleOf2Atoms(a_atomdata[a_bonds [j].fromAtomID - 1 ], obj_atms[1], a_atomdata[a_bonds [j].fromAtomID - 1 ], obj_atms[2] );
							double o_angle3 = AngleOf2Atoms(a_atomdata[a_bonds [j].fromAtomID - 1 ], obj_atms[2], a_atomdata[a_bonds [j].fromAtomID - 1 ], obj_atms[0] );


							double o_angle = (Math.Abs(o_angle1) + Math.Abs(o_angle2) + Math.Abs(o_angle3))/ 3.0;

							Console.WriteLine("check bond order C~" + a_atomdata[a_bonds [j].toAtomID - 1 ].atom_site_type_symbol);
							Console.WriteLine("angle1:" + o_angle1);
							Console.WriteLine("angle2:" + o_angle2);
							Console.WriteLine("angle3:" + o_angle3);
							Console.WriteLine("av. angle:" + o_angle);

							string log_length = String.Empty;
							double length = 0.0;
							int o_order1 = bondtype4length(a_atomdata[a_bonds [j].fromAtomID - 1 ], obj_atms[0], ref  length, ref log_length);
							int o_order2 = bondtype4length(a_atomdata[a_bonds [j].fromAtomID - 1 ], obj_atms[1], ref  length, ref log_length);
							int o_order3 = bondtype4length(a_atomdata[a_bonds [j].fromAtomID - 1 ], obj_atms[2], ref  length, ref log_length);

							System.Diagnostics.Debug.WriteLine (log_length);
							log_sb.AppendLine (log_length);

							Console.WriteLine("bond order 1:" + o_order1);
							Console.WriteLine("bond order 2:" + o_order2);
							Console.WriteLine("bond order 3:" + o_order3);


							if (o_angle < 125 && o_angle > 115){ //この条件では、C=Nと誤変換する場合がある。
								// 他の結合が二重結合でない場合のみとする必要がある。
								if (o_order1 != 2 && o_order2 != 2 && o_order3 != 2) {
									a_bonds [j].BondOrder = 2;
									Console.WriteLine("change bond order to 2");
								}
							}
							else if (o_angle <= 115) {
								a_bonds [j].BondOrder = 1;
								Console.WriteLine("change bond order to 1");
							}

						}
					}
					}
					catch (Exception ex){
						log_sb.AppendLine (ex.ToString ());
						Console.WriteLine (ex.ToString ());
					}
			
			}
			// log_sb.AppendLine (ex.ToString ());

			// < 110 angle => sp3(C-O), 110<= angle < 150? => sp2(C=O), 150?> => sp(C#O)
		}



		/// <summary>
		/// get connect atoms
		/// </summary>
		/// <returns>The around atoms.</returns>
		/// <param name="a_at">A at.</param>
		/// <param name="a_atomdata">A atomdata.</param>
		/// <param name="a_bonds">A bonds.</param>
		public static List<Atom> GetConnectAtoms(ref StringBuilder log_sb, Atom a_at, List<Atom> a_atomdata, List<Bond> a_bonds){
			List<Atom> ar_atoms = new List<Atom> ();
			List<int> obj_ids = new List<int> ();
			try {
				for (int j = 0; j < a_bonds.Count; j++) {
			
					if (a_bonds [j].fromAtomID == a_at.id) {
						if (obj_ids.Contains (a_bonds [j].toAtomID)) {
							obj_ids.Add (a_bonds [j].toAtomID);
						}
					} else if (a_bonds [j].toAtomID == a_at.id) {
						if (obj_ids.Contains (a_bonds [j].fromAtomID)) {
							obj_ids.Add (a_bonds [j].fromAtomID);
						}
					}
				}

				for (int i = 0; i < a_atomdata.Count; i++) {
					if (obj_ids.Contains (a_atomdata [i].id)) {
						ar_atoms.Add (a_atomdata [i]);
					}
				}
			} catch (Exception ex) {
				log_sb.AppendLine (ex.ToString ());
				Console.WriteLine (ex.ToString ());
			}

			return ar_atoms;
		}


		public static bool checkTerminalHeteroAtom(Atom a_target_atom, List<Atom> atoms, List<Bond> bonds){
			bool bl_TerminalHeteroAtom = false;
			int targetAtomBondCount = 0;

			for (int i = 0; i < atoms.Count; i++) {

				if (atoms [i].atom_site_type_symbol != "H") {
					if (atoms [i].atom_site_id == a_target_atom.atom_site_id) {

						for (int j = 0; j < bonds.Count; j++) {

							if (bonds [j].fromAtomID == atoms [i].id || bonds [j].toAtomID == atoms [i].id) {
								targetAtomBondCount++;
							}
						}
					}
				}
			}

			if (targetAtomBondCount == 1) {
				bl_TerminalHeteroAtom = true;
			}

			return bl_TerminalHeteroAtom;
		}

		/// <summary>
		/// checking Valencies for C,H,N,O
		/// </summary>
		/// <returns><c>true</c>, if valencies was checked, <c>false</c> otherwise.</returns>
		public static bool checkValencies(ref StringBuilder local_error_sb, ref StringBuilder a_local_error_sb, List<Atom> atoms, List<Bond> bonds){
			bool bl_Valencie = true;

			// C_Valencie < 5
			// H_Valencie == 1
			// O_Valencie < 3
			// N_Valencie < 5

			for (int i = 0; i < atoms.Count; i++) {


				if (i == 29) {
					Console.WriteLine ("STOP");
				}

				int int_bondValence = 0;
				for (int j = 0; j < bonds.Count; j++) {

					if (bonds [j].fromAtomID == atoms[i].id || bonds [j].toAtomID == atoms[i].id) {
						int_bondValence += bonds [j].BondOrder;
						if (i == 29) {
							Console.WriteLine ("Bond order: " + j + ":" + bonds [j].BondOrder);
							Console.WriteLine ("Bond: " + j + ":" + bonds [j].fromAtomID + "-" + bonds [j].toAtomID);
							Console.WriteLine ("Bond: " + j + ":" + bonds [j].fromAtom + "-" + bonds [j].toAtom);
						}
					}
				}

				// atom_site_label_atom_id_Position	"C1" 	string
				// atom_site_label_comp_id_molCode 	"MAN"	string

				if (atoms [i].atom_site_type_symbol == "C") {
					if (int_bondValence > 4) {
						bl_Valencie = false;
						a_local_error_sb.AppendLine ("Valence error of " + atoms [i].atom_site_type_symbol + ":\t" + "\t" 
							+ atoms [i].atom_site_id+ "\t"   // 1
							+ atoms [i].atom_site_label_comp_id_molCode + "\t"  // NAG
							+ String.Format ("{0,6}", atoms [i].atom_site_label_atom_id_Position) + "\t"  // C1
							+ atoms [i].atom_site_occupancy + "\t"
							+ int_bondValence
						);
						//break;
					}
				}
				else if (atoms [i].atom_site_type_symbol == "H") {

					if (int_bondValence !=  1) {
						bl_Valencie = false;
						a_local_error_sb.AppendLine ("Valence error of " + atoms [i].atom_site_type_symbol + ":\t" + "\t" 
							+ atoms [i].atom_site_id+ "\t"   // 1
							+ atoms [i].atom_site_label_comp_id_molCode + "\t"  // NAG
							+ String.Format ("{0,6}", atoms [i].atom_site_label_atom_id_Position) + "\t"  // C1
							+ atoms [i].atom_site_occupancy + "\t"
							+ int_bondValence
						);
						//break;
					}
				}
				else if (atoms [i].atom_site_type_symbol == "O") {
					if (int_bondValence > 2) {
						bl_Valencie = false;
						a_local_error_sb.AppendLine ("Valence error of " + atoms [i].atom_site_type_symbol + ":\t" + "\t" 
							+ atoms [i].atom_site_id+ "\t"   // 1
							+ atoms [i].atom_site_label_comp_id_molCode + "\t"  // NAG
							+ String.Format ("{0,6}", atoms [i].atom_site_label_atom_id_Position) + "\t"  // C1
							+ atoms [i].atom_site_occupancy + "\t"
							+ int_bondValence
						);
						//break;
					}
				}
				else if (atoms [i].atom_site_type_symbol == "N") {
					if (int_bondValence > 4) {
						bl_Valencie = false;
						a_local_error_sb.AppendLine ("Valence error of " + atoms [i].atom_site_type_symbol + ":\t" + "\t" 
							+ atoms [i].atom_site_id+ "\t"   // 1
							+ atoms [i].atom_site_label_comp_id_molCode + "\t"  // NAG
							+ String.Format ("{0,6}", atoms [i].atom_site_label_atom_id_Position) + "\t"  // C1
							+ atoms [i].atom_site_occupancy + "\t"
							+ int_bondValence
						);
						//break;
					}
				}

			}
			return bl_Valencie;
		}


		/// <summary>
		/// at.id = atomNumber; atomNumber++;
		/// </summary>
		/// <param name="atoms">Atoms.</param>
		/// <param name="bonds">Bonds.</param>
		public static void checkBonds(ref List<Atom> atoms, ref List<Bond> bonds, ref String log){

			//原子に結合している結合を格納
			List<int> bondingAtoms = new List<int>();
			for (int j = 0; j < bonds.Count; j++) {
				if (!bondingAtoms.Contains(bonds[j].fromAtomID)){
					bondingAtoms.Add(bonds[j].fromAtomID);
				}
				if (!bondingAtoms.Contains(bonds[j].toAtomID)){
					bondingAtoms.Add(bonds[j].toAtomID);
				}
			}
			//Console.WriteLine("set bond to atom");
			//outsb.AppendLine ("set bond to atom");

			//結合のある原子のみを抽出
			List<Atom> atomdata = new List<Atom> ();
			//Console.WriteLine("start bonding atom check: " + atoms.Count);
			//outsb.AppendLine ("start bonding atom check: " + atoms.Count);

			int atomNumber = 1;
			for (int i = 0; i < atoms.Count; i++) {

				//Console.WriteLine("bond count: " + atoms[i].Bonds.Count);

				if (bondingAtoms.Contains(atoms[i].id)) {
					Atom at = new Atom();

					//at = atoms [i];
					at =  GlycoNAVI.AtomUtility.SetAtomData (atoms [i]);
					at.X = atoms[i].X;
					at.Y = atoms[i].Y;
					at.Z = atoms[i].Z;
					at.atom_site_type_symbol = atoms[i].atom_site_type_symbol;
					at.atom_site_label_atom_id_Position = atoms[i].atom_site_label_atom_id_Position;
					at.atom_site_label_comp_id_molCode = atoms[i].atom_site_label_comp_id_molCode;
					at.atom_site_label_asym_id = atoms [i].atom_site_label_asym_id;
					at.atom_site_label_entity_id = atoms [i].atom_site_label_entity_id;
					at.seq_id = atoms [i].seq_id;
					at.atom_site_id = atoms [i].atom_site_id;
					at.id = atomNumber;
					at.atom_site_label_alt_id = atoms [i].atom_site_label_alt_id;
					atomNumber++;
					atomdata.Add (at);
				}
			}
			//Console.WriteLine("choose bonding atom");
			//outsb.AppendLine("choose bonding atom");

			//結合情報を再度確認
			bonds = new List<Bond> ();
			//bond-order
			int bondid = 1;

			for (int i = 0; i < atomdata.Count; i++) {
			//foreach (var a1 in atomdata) {
				for (int j = 0; j < atomdata.Count; j++) {
				//foreach (var a2 in atomdata) {
					if (atomdata[i].id < atomdata[j].id) {
						double length = 0.0;
						int order = Chemical.bondtype4length (atomdata[i], atomdata[j], ref length, ref log);
						if (order > 0) {
							Bond bd = new Bond ();
							bd.id = bondid;
							bondid++;
							bd.BondOrder = order;
							bd.fromAtomID = atomdata[i].id;
							bd.fromAtom = atomdata[i].atom_site_type_symbol;
							bd.toAtomID = atomdata[j].id;
							bd.toAtom = atomdata[j].atom_site_type_symbol;
							bd.length = length;
							bonds.Add (bd);
						}
					}
				}
			}
		}

		public static void checkAcetamideStructure(ref StringBuilder log_sb, ref StringBuilder local_error_sb, ref List<Atom> atomdata, ref List<Bond> bonds, string idString){


			for (int i = 0; i < atomdata.Count; i++) {
				bool terminalatom = checkTerminalHeteroAtom (atomdata [i], atomdata, bonds);
				if (terminalatom) {
					atomdata [i].is_terminal_heteroatom = true;
				} else {
					atomdata [i].is_terminal_heteroatom = false;
				}
			}
/*
			//原子に結合している結合を格納
			List<int> bondingAtoms = new List<int>();
			for (int j = 0; j < bonds.Count; j++) {
				if (!bondingAtoms.Contains(bonds[j].fromAtomID)){
					bondingAtoms.Add(bonds[j].fromAtomID);
				}
				if (!bondingAtoms.Contains(bonds[j].toAtomID)){
					bondingAtoms.Add(bonds[j].toAtomID);
				}
			}
			//Console.WriteLine("set bond to atom");
			//outsb.AppendLine ("set bond to atom");

			//結合のある原子のみを抽出
			List<Atom> atomdata = new List<Atom> ();
			//Console.WriteLine("start bonding atom check: " + atoms.Count);
			//outsb.AppendLine ("start bonding atom check: " + atoms.Count);

			int atomNumber = 1;
			for (int i = 0; i < atoms.Count; i++) {

				//Console.WriteLine("bond count: " + atoms[i].Bonds.Count);

				if (bondingAtoms.Contains(atoms[i].id)) {
					Atom at = new Atom();
					at = atoms [i];
					at.X = atoms[i].X;
					at.Y = atoms[i].Y;
					at.Z = atoms[i].Z;
					at.type_symbol = atoms[i].type_symbol;
					at.atom_id_Position = atoms[i].atom_id_Position;
					at.comp_id_molCode = atoms[i].comp_id_molCode;
					at.asym_id = atoms [i].asym_id;
					at.entity_id = atoms [i].entity_id;
					at.seq_id = atoms [i].seq_id;
					at.atom_index = atoms [i].atom_index;
					at.id = atomNumber;
					atomNumber++;
					atomdata.Add (at);
				}
			}
			//Console.WriteLine("choose bonding atom");
			//outsb.AppendLine("choose bonding atom");

			//結合情報を再度確認
			bonds = new List<Bond> ();
			//bond-order
			int bondid = 1;
			foreach (var a1 in atomdata) {
				foreach (var a2 in atomdata) {
					if (a1.id < a2.id) {
						double length = 0.0;
						int order = Chemical.bondtype4length (a1, a2, ref length);
						if (order > 0) {
							Bond bd = new Bond ();
							bd.id = bondid;
							bondid++;
							bd.BondType = order;
							bd.fromAtomID = a1.id;
							bd.fromAtom = a1.type_symbol;
							bd.toAtomID = a2.id;
							bd.toAtom = a2.type_symbol;
							bd.length = length;
							bonds.Add (bd);
						}
					}
				}
			}

*/

			//Console.WriteLine("check bond length information");
			//outsb.AppendLine("check bond length information");

			//結合角と結合長を考慮した結合次数の決定

			//Bond Listをconnectに格納
			List<List<Bond>> connect = new List<List<Bond>> ();

			for (int i = 0; i < atomdata.Count; i++) {
				List<Bond> bd = new List<Bond> ();
				for (int j = 0; j < bonds.Count; j++) {
					if (atomdata [i].id == bonds [j].fromAtomID || atomdata [i].id == bonds [j].toAtomID) {
						//if (atomdata [i].hetAtom) {
						bd.Add (bonds [j]);
						//}
					}
				}
				if (bd.Count > 0) {
					connect.Add (bd);
				}
			}

			//周りの原子との結合の数と結合角から結合を再度検討

			// -NHC(=O)CH3の C-N が C=N となっている。O=C(=NC)C の構造があったら、C-NH-C(=O)-C とする。
			// この際、アミド結合の cis-,trans-を確認し、trans-であれば、OとCの元素記号を交換する
			//
			//      C*			      C*                    C*
			//       \			       \                     \
			//        N				   N                     N-H
			//        || 			   ||                    |
			//        C* - C           C* - O*               C* - C
			//       //               /                     //
			//      O*               C                     O
			//
			//      trans-amide    cis-amide               trans-amide
			//      stable         unstable                stable
			//      C=N (x)        C=N (x)
			//


			int count = 1;
			List<List<int>> amid_CNs = new List<List<int>> ();
			List<List<int>> carboxy_COs = new List<List<int>> ();
			//Console.WriteLine("connect count: " + connect.Count);
			foreach (var c in connect) {
				int connectNumber = 0;
				//string data = string.Empty;
				List<int> ids = new List<int> ();
				List<int> carboxy_ids = new List<int> ();
				foreach (var b in c) {
					//Console.WriteLine(count + " bond: " + b.fromAtomID + "-" + b.toAtomID + " : " + b.BondType);

					/*
										data += count + ">>>>>> bond: " 
											+ b.id + " : "
											+ b.fromAtomID + "(" + b.fromAtom + ")-" 
											+ b.toAtomID + "(" + b.toAtom + ")" + ": " 
											+ b.BondType + "\r\n";
										*/

					//原子周りの結合数の追加
					connectNumber += b.BondOrder;

					if ((b.fromAtom == "N" && b.toAtom == "C") || (b.fromAtom == "C" && b.toAtom == "N")) {
						ids.Add (b.id);
					} else if ((b.fromAtom == "O" && b.toAtom == "C") || (b.fromAtom == "C" && b.toAtom == "O")) {
						carboxy_ids.Add (b.id);
					}
				}
				count++;
				//Console.WriteLine(count + " connectNumber: " + connectNumber);
				if (connectNumber > 4) {
					//Console.WriteLine(count + " connectNumber: " + connectNumber);
					//Console.WriteLine(data);
					if (ids.Count > 0) {
						amid_CNs.Add (ids);
					}
					if (carboxy_COs.Count > 0) {
						carboxy_COs.Add (carboxy_ids);
					}
				}

			}

			/*
								count = 1;
								Console.WriteLine(">> amid_CNs");
								foreach (var am in amid_CNs){
									Console.WriteLine("# count: " + count);
									foreach (var a in am){
										Console.WriteLine(a);
									}
								}
								Console.WriteLine("<< amid_CNs");
								*/





			//								* A1         B1  A1         *   A1         *
			//								*   \       /      \       /      \       /
			//								*    A0 - B0        A0 - B0        A0 - B0
			//							 	*   /       \      /       \      /       \
			//								*  *         *    *         B1   *         *
			//Atom A0, Atom A1, Atom B0, Atom B1, Atom B2 = CH3 of Ac
			//Atom N, Atom C, Atom C*, Atom O*
			List<Amide> amideUnit = new List<Amide> ();

			count = 1;
			foreach (var c in amid_CNs) {
				Amide amd = new Amide ();

				foreach (var id_t in c) {

					int id = id_t - 1;  //配列が"0"から始まるため

					//Console.WriteLine("id in c: " + id);

					//Console.Write("amid: " + bonds[id].fromAtom + "(" + bonds[id].fromAtomID + ")");
					//Console.WriteLine(" - " + bonds[id].toAtom + "(" + bonds[id].toAtomID + ")");

					if (bonds [id].fromAtom == "N" && bonds [id].toAtom == "C") {
						amd.A0 = bonds [id].fromAtomID;
						amd.B0 = bonds [id].toAtomID;

						foreach (var at in connect[amd.A0 - 1]) {
							if (at.fromAtomID == amd.A0 && at.toAtom == "C" && at.toAtomID != amd.B0) {
								amd.A1 = at.toAtomID;
								continue;
							} else if (at.toAtomID == amd.A0 && at.fromAtom == "C" && at.fromAtomID != amd.B0) {
								amd.A1 = at.fromAtomID;
								continue;
							}
						}

						foreach (var at in connect[amd.B0 - 1]) {
							if (at.fromAtomID == amd.B0 && at.toAtom == "O") {
								amd.B1 = at.toAtomID;
								continue;
							} else if (at.toAtomID == amd.B0 && at.fromAtom == "O") {
								amd.B1 = at.fromAtomID;
								continue;
							} else if (at.fromAtomID == amd.B0 && at.toAtom == "C" && at.toAtomID != amd.A0) {
								amd.B2 = at.toAtomID;
								continue;
							} else if (at.toAtomID == amd.B0 && at.fromAtom == "C" && at.fromAtomID != amd.A0) {
								amd.B2 = at.fromAtomID;
								continue;
							}
						}

					} else if (bonds [id].fromAtom == "C" && bonds [id].toAtom == "N") {
						amd.B0 = bonds [id].fromAtomID;
						amd.A0 = bonds [id].toAtomID;

						foreach (var at in connect[amd.A0 - 1]) {
							if (at.fromAtomID == amd.A0 && at.toAtom == "C" && at.toAtomID != amd.B0) {
								amd.A1 = at.toAtomID;
								continue;
							} else if (at.toAtomID == amd.A0 && at.fromAtom == "C" && at.fromAtomID != amd.B0) {
								amd.A1 = at.fromAtomID;
								continue;
							}
						}

						foreach (var at in connect[amd.B0 - 1]) {
							if (at.fromAtomID == amd.B0 && at.toAtom == "O") {
								amd.B1 = at.toAtomID;
								continue;
							} else if (at.toAtomID == amd.B0 && at.fromAtom == "O") {
								amd.B1 = at.fromAtomID;
								continue;
							} else if (at.fromAtomID == amd.B0 && at.toAtom == "C" && at.toAtomID != amd.A0) {
								amd.B2 = at.toAtomID;
								continue;
							} else if (at.toAtomID == amd.B0 && at.fromAtom == "C" && at.fromAtomID != amd.A0) {
								amd.B2 = at.fromAtomID;
								continue;
							}
						}
					}
				}
				amd.id = count;
				count++;
				amideUnit.Add (amd);
			}

			count = 1;
			// trans-アミドならはSynとなる。
			foreach (var am in amideUnit) {

				Atom A0 = new Atom ();
				Atom A1 = new Atom ();
				Atom B0 = new Atom ();
				Atom B1 = new Atom ();
				Atom B2 = new Atom ();

				foreach (var at in atomdata) {
					if (at.id == am.A0) {
						A0 = at;
					} else if (at.id == am.A1) {
						A1 = at;
					} else if (at.id == am.B0) {
						B0 = at;
					} else if (at.id == am.B1) {
						B1 = at;
					} else if (at.id == am.B2) {
						B2 = at;
					}
				}

				string cistrans = Chemical.synanti (A0, A1, B0, B1);

				if (cistrans == "Syn") {
					//Console.WriteLine(count + ": trans-amid");
					count++;
				} else if (cistrans == "Anti") {
					//Console.WriteLine("?????" + count + ": cis-amid");

					count++;

					log_sb.AppendLine ("Please check a geometry of amide group.: " + idString + "." + A0.atom_site_label_entity_id + "." + A0.atom_site_label_asym_id);
					local_error_sb.AppendLine ("Please check a geometry of amide group.: " + idString + "." + A0.atom_site_label_entity_id + "." + A0.atom_site_label_asym_id);




					//NHAcである場合、cis-をtransにする処理を追加する。
					//C と　O の元素記号と結合次数をC(B0)-C & C(B0)=O とする。

					local_error_sb.AppendLine (
						"[A0]("
						+ String.Format ("{0,10:0.0000}", A0.X) + ","
						+ String.Format ("{0,10:0.0000}", A0.Y) + ","
						+ String.Format ("{0,10:0.0000}", A0.Z) + ") "
						+ A0.atom_site_type_symbol + "["
						+ String.Format ("{0,3}", A0.id) + "] Code:"
						+ A0.atom_site_label_comp_id_molCode + " Position:"
						+ A0.atom_site_label_atom_id_Position
						+ " entry_id:"
						+ A0.atom_site_label_entity_id
						+ " asym_id:"
						+ A0.atom_site_label_asym_id
						+ " atom_site_id:"
						+ A0.atom_site_id
					);

					local_error_sb.AppendLine (
						"[A1]("
						+ String.Format ("{0,10:0.0000}", A1.X) + ","
						+ String.Format ("{0,10:0.0000}", A1.Y) + ","
						+ String.Format ("{0,10:0.0000}", A1.Z) + ") "
						+ A1.atom_site_type_symbol + "["
						+ String.Format ("{0,3}", A1.id) + "] Code:"
						+ A1.atom_site_label_comp_id_molCode + " Position:"
						+ A1.atom_site_label_atom_id_Position
						+ " entry_id:"
						+ A1.atom_site_label_entity_id
						+ " asym_id:"
						+ A1.atom_site_label_asym_id
						+ " atom_site_id:"
						+ A1.atom_site_id
					);

					local_error_sb.AppendLine (
						"[B0]("
						+ String.Format ("{0,10:0.0000}", B0.X) + ","
						+ String.Format ("{0,10:0.0000}", B0.Y) + ","
						+ String.Format ("{0,10:0.0000}", B0.Z) + ") "
						+ B0.atom_site_type_symbol + "["
						+ String.Format ("{0,3}", B0.id) + "] Code:"
						+ B0.atom_site_label_comp_id_molCode + " Position:"
						+ B0.atom_site_label_atom_id_Position
						+ " entry_id:"
						+ B0.atom_site_label_entity_id
						+ " asym_id:"
						+ B0.atom_site_label_asym_id
						+ " atom_site_id:"
						+ B0.atom_site_id
					);

					local_error_sb.AppendLine (
						"[B1]("
						+ String.Format ("{0,10:0.0000}", B1.X) + ","
						+ String.Format ("{0,10:0.0000}", B1.Y) + ","
						+ String.Format ("{0,10:0.0000}", B1.Z) + ") "
						+ B1.atom_site_type_symbol + "["
						+ String.Format ("{0,3}", B1.id) + "] Code:"
						+ B1.atom_site_label_comp_id_molCode + " Position:"
						+ B1.atom_site_label_atom_id_Position
						+ " entry_id:"
						+ B1.atom_site_label_entity_id
						+ " asym_id:"
						+ B1.atom_site_label_asym_id
						+ " atom_site_id:"
						+ B1.atom_site_id
					);

					local_error_sb.AppendLine (
						"[B2]("
						+ String.Format ("{0,10:0.0000}", B2.X) + ","
						+ String.Format ("{0,10:0.0000}", B2.Y) + ","
						+ String.Format ("{0,10:0.0000}", B2.Z) + ") "
						+ B2.atom_site_type_symbol + "["
						+ String.Format ("{0,3}", B2.id) + "] Code:"
						+ B2.atom_site_label_comp_id_molCode + " Position:"
						+ B2.atom_site_label_atom_id_Position
						+ " entry_id:"
						+ B2.atom_site_label_entity_id
						+ " asym_id:"
						+ B2.atom_site_label_asym_id
						+ " atom_site_id:"
						+ B2.atom_site_id
					);


					//B1, B2を座標はそのままで元素記号を入れ替える。


					// 変更をアセタミドのみに限定する。
					if (B1.is_terminal_heteroatom == true && B2.is_terminal_heteroatom == true) {


						//Console.WriteLine(">>> B1.Position: " + B1.Position);
						//Console.WriteLine(">>> B2.Position: " + B2.Position);

						for (int i = 0; i < atomdata.Count; i++) {
							if (atomdata [i].id == B1.id) {
								//Console.WriteLine("B1 -> B2");
								Atom at = new Atom ();
								//at = B2;
								at =  GlycoNAVI.AtomUtility.SetAtomData (B2);

								at.id = B1.id;
								at.X = B1.X;
								at.Y = B1.Y;
								at.Z = B1.Z;



								at.atom_site_label_asym_id = B2.atom_site_label_asym_id;
								at.atom_site_label_entity_id = B2.atom_site_label_entity_id;
								at.seq_id = B2.seq_id;
								at.atom_site_id = B2.atom_site_id;


								at.atom_site_label_comp_id_molCode = B2.atom_site_label_comp_id_molCode;
								at.atom_site_label_atom_id_Position = B2.atom_site_label_atom_id_Position;
								at.atom_site_type_symbol = B2.atom_site_type_symbol;
								atomdata [i] = at;

								//Console.WriteLine("   B1->B2: " + B2.Position + ": " + B2.Symbol);
								//Console.WriteLine("at:B1->B2: " + at.Position + ": " + at.Symbol);

							} else if (atomdata [i].id == B2.id) {
								Atom at = new Atom ();
								//at = B1;
								at =  GlycoNAVI.AtomUtility.SetAtomData (B1);

								at.id = B2.id;
								at.X = B2.X;
								at.Y = B2.Y;
								at.Z = B2.Z;

								at.atom_site_label_asym_id = B1.atom_site_label_asym_id;
								at.atom_site_label_entity_id = B1.atom_site_label_entity_id;
								at.seq_id = B1.seq_id;
								at.atom_site_id = B1.atom_site_id;


								at.atom_site_label_comp_id_molCode = B1.atom_site_label_comp_id_molCode;
								at.atom_site_label_atom_id_Position = B1.atom_site_label_atom_id_Position;
								at.atom_site_type_symbol = B1.atom_site_type_symbol;
								atomdata [i] = at;

								//Console.WriteLine("   B2->B1: " + B1.Position + ": " + B1.Symbol);
								//Console.WriteLine("at:B1->B2: " + at.Position + ": " + at.Symbol);

							}
						}

						//B0=B1 -> B0-B1; B0-B2 -> B0=B2の結合次数を変更する。
						for (int j = 0; j < bonds.Count; j++) {
							if ((bonds [j].fromAtomID == B0.id && bonds [j].toAtomID == B1.id)
							    ||
							    (bonds [j].fromAtomID == B1.id && bonds [j].toAtomID == B0.id)) {
								bonds [j].BondOrder = 1;
							}
							if ((bonds [j].fromAtomID == B0.id && bonds [j].toAtomID == B2.id)
							    ||
							    (bonds [j].fromAtomID == B2.id && bonds [j].toAtomID == B0.id)) {
								bonds [j].BondOrder = 2;
							}
						}


						//変更したことをログに残す。
						log_sb.AppendLine ("structure changed from cis-amid to trans-amid form.: " + idString + "." + A0.atom_site_label_entity_id + "." + A0.atom_site_label_asym_id);
						log_sb.AppendLine ("Please check amid atoms: "
						+ A0.atom_site_label_comp_id_molCode + ","
						+ A0.atom_site_label_atom_id_Position + "; "
						+ A1.atom_site_label_comp_id_molCode + ","
						+ A1.atom_site_label_atom_id_Position + "; "
						+ B0.atom_site_label_comp_id_molCode + ","
						+ B0.atom_site_label_atom_id_Position + "; "
						+ B1.atom_site_label_comp_id_molCode + ","
						+ B1.atom_site_label_atom_id_Position + "; "
						);




					}













				} else {
					//Console.WriteLine(count + ": ?-amid");
					count++;
				}
















				//* A1         B1  A1         *   A1         *
				//*   \       /      \       /      \       /
				//*    A0 - B0        A0 - B0        A0 - B0
				//*   /       \      /       \      /       \
				//*  *         *    *         B1   *         *
				//Atom A0, Atom A1, Atom B0, Atom B1
				//Atom N, Atom C, Atom C*, Atom O*


				//c1-n-c2-oc
				//(Atom c1, Atom n, Atom c2, Atom oc) 
				//double ip = Calculation.amidVec3dinnerProduct(atomdata[am.A1], atomdata[am.A0], atomdata[am.B0], atomdata[am.B1]);
				//Console.WriteLine("amidVec3dinnerProduct: " + ip);

			}
			//Console.WriteLine("<< end amide unit cis or trans ?");





			//amide C-N bond to single
			for (int j = 0; j < bonds.Count; j++) {
				for (int i = 0; i < amid_CNs.Count; i++) {
					if (amid_CNs [i].Contains (bonds [j].id)) {
						bonds [j].BondOrder = 1;
					}
				}
			}






			/*
								foreach (var cs in connect){

								}


								for (int j = 0; j < bonds.Count; j++) {
									for (int i = 0; i < amid_CNs.Count; i++) {
										if (amid_CNs[i].Contains(bonds[j].id)){


										}
									}
								}
								*/





			// carboxyl group -C(=O)=O to -C(=O)O
			//
			List<int> selectCO2 = new List<int> ();
			foreach (var c in carboxy_COs) {
				//Console.WriteLine("carboxy_CO count: " + c.Count);
				if (c.Count == 2) {
					foreach (var a in c) {
						//Console.WriteLine("carboxy_COs: " + a);
						selectCO2.Add (a);
						break;
					}
				}
			}

			/*
								foreach (var c in selectCO2){
									Console.WriteLine("CO2: " + c);
								}
								*/

			//carboxyl C=O bond to single
			for (int j = 0; j < bonds.Count; j++) {
				if (selectCO2.Contains (bonds [j].id)) {
					bonds [j].BondOrder = 1;
				}
			}

		}

		//２つのベクトルABのなす角度θを求める
		public static double AngleOf2Atoms(Atom A0, Atom A1, Atom B0, Atom B1 ){

			Vector.Vector3D A = new Vector.Vector3D();
			A.x = A0.X - A1.X;
			A.y = A0.Y - A1.Y;
			A.z = A0.Z - A1.Z;

			Vector.Vector3D B = new Vector.Vector3D();
			B.x = B0.X - B1.X;
			B.y = B0.Y - B1.Y;
			B.z = B0.Z - B1.Z;

			double shita = Vector.AngleOf2Vector (A, B);
			return shita;

		}

		private static double EPS = 0.0000001;

		/*
	 	* Stereo chemistry for double bond.
	 	* <pre>
	 	* Syn            Anti           N
	 	* A1         B1  A1         *   A1         *
	 	*   \       /      \       /      \       /
	 	*    A0 = B0        A0 = B0        A0 = B0
	 	*   /       \      /       \      /       \
		*  *         *    *         B1   *         *
		* </pre>
		* @param A0
		* @param A1
		* @param B0
		* @param B1
		* @return stereo("Syn", "Anti" or "X")
			*/
			public static String synanti(Atom A0, Atom A1, Atom B0, Atom B1){
			/*
			for(Connection connection : A0.connections){
				if(connection.stereo==3 || connection.stereo==4){
					return "X";
				}
			}
			for(Connection connection : B0.connections){
				if(connection.stereo==3 || connection.stereo==4){
					return "X";
				}
			}
			*/
			if(B1 == null) return "N";

			double result = Calculation.innerProduct(Calculation.outerProduct(A0, A1, B0), Calculation.outerProduct(B0, A0, B1));

			//Console.WriteLine ("synanti: " + result);

			if(Math.Abs(result) < EPS){
				return "X";
			}
			return (result > 0) ? "Syn" : "Anti";
		}



		public static double synantiDouble(Atom A0, Atom A1, Atom B0, Atom B1){
			//if(B1 == null) return "N";

			return Calculation.innerProduct(Calculation.outerProduct(A0, A1, B0), Calculation.outerProduct(B0, A0, B1));
		}


		public static string checkAtomicSymbol(string sym){

			string AtomS = string.Empty;

			foreach (var a in ChemicalSymbols) {
				if (sym.ToUpper ().Equals (a.ToUpper ())) {
					AtomS = a;
					break;
				}
			}

			return AtomS;
		}

		#region ChemicalSymbols
		public static string[] ChemicalSymbols = 
		{
			"H"
			, "He"
			, "Li"
			, "Be"
			, "B"
			, "C"
			, "N"
			, "O"
			, "F"
			, "Ne"
			, "Na"
			, "Mg"
			, "Al"
			, "Si"
			, "P"
			, "S"
			, "Cl"
			, "Ar"
			, "K"
			, "Ca"
			, "Sc"
			, "Ti"
			, "V"
			, "Cr"
			, "Mn"
			, "Fe"
			, "Co"
			, "Ni"
			, "Cu"
			, "Zn"
			, "Ga"
			, "Ge"
			, "As"
			, "Se"
			, "Br"
			, "Kr"
			, "Rb"
			, "Sr"
			, "Y"
			, "Zr"
			, "Nb"
			, "Mo"
			, "Tc"
			, "Ru"
			, "Rh"
			, "Pd"
			, "Ag"
			, "Cd"
			, "In"
			, "Sn"
			, "Sb"
			, "Te"
			, "I"
			, "Xe"
			, "Cs"
			, "Ba"
			, "La"
			, "Ce"
			, "Pr"
			, "Nd"
			, "Pm"
			, "Sm"
			, "Eu"
			, "Gd"
			, "Tb"
			, "Dy"
			, "Ho"
			, "Er"
			, "Tm"
			, "Yb"
			, "Lu"
			, "Hf"
			, "Ta"
			, "W"
			, "Re"
			, "Os"
			, "Ir"
			, "Pt"
			, "Au"
			, "Hg"
			, "Tl"
			, "Pb"
			, "Bi"
			, "Po"
			, "At"
			, "Rn"
			, "Fr"
			, "Ra"
			, "Ac"  //Actinium
			, "Th"
			, "Pa"
			, "U"
			, "Np"
			, "Pu"
			, "Am"
			, "Cm"
			, "Bk"
			, "Cf"
			, "Es"
			, "Fm"
			, "Md"
			, "No"
			, "Lr"
			, "A"
			, "*"
			, "Q"
			, "X"
		};
		#endregion ChemicalSymbols

		/// <summary>
		/// Bondtype between a1 and a2. single:1, double:2, triple:3, aromatic:4
		/// </summary>
		/// <param name="a1">A1.</param>
		/// <param name="a2">A2.</param>
		public static int bondtype4length(Atom a1, Atom a2, ref double length, ref string log_length){
			int bondorder = 0;

			//原子間距離
			length = Calculation.length3D (a1, a2);

			// http://www.h6.dion.ne.jp/~k-sugino/modeling/covalent.html

			// type vdw+ PDB(3AIC)
			// C-C	1.50 1.45-1.65
			// C=C 1.34 1.3-1.45
			// C-N 1.46 1.4-1.7
			// C=N 1.27 1.25-1.4
			// C-O 1.38 1.3-1.55
			// C=O 1.24 1.15-1.3
			// N-N 1.42
			// N=N 1.20
			// N-O 1.34
			// N=O 1.17
			// O-O 1.26
			// O=O 1.14

			float vdw_pdb_factor = 1.2f;
			//各原子のVDWの和
			double vdwvdw = ((VDW (a1.atom_site_type_symbol) + VDW (a2.atom_site_type_symbol)) * 0.5f) * vdw_pdb_factor;

			//比較
			if (length < vdwvdw) {


				if ((a1.atom_site_type_symbol == "C" && a2.atom_site_type_symbol == "O") || (a1.atom_site_type_symbol == "O" && a2.atom_site_type_symbol == "C")) {
					if (length < 1.28 && length > 1.1) {
						bondorder = 2;
					} else if (length >= 1.28) {
						bondorder = 1;
					}
				} else if ((a1.atom_site_type_symbol == "C" && a2.atom_site_type_symbol == "N") || (a1.atom_site_type_symbol == "N" && a2.atom_site_type_symbol == "C")) {
					if (length < 1.35 && length >= 1.2) {
						bondorder = 2;
					} else if (length < 1.2 && length > 1.0) {
						bondorder = 3;
					} else if (length >= 1.35) {
						bondorder = 1;
					}
				} else if (a1.atom_site_type_symbol == "C" && a2.atom_site_type_symbol == "C") {
					if (length < 1.3) {
						bondorder = 3;
					} else if (length < 1.4) {
						bondorder = 2;
					} else if (length >= 1.4) {
						bondorder = 1;
					}
				} else if (a1.atom_site_type_symbol == "N" && a2.atom_site_type_symbol == "N") {
					if (length < 1.3) {
						bondorder = 2;
					} else if (length >= 1.3) {
						bondorder = 1;
					}
				} else if (a1.atom_site_type_symbol == "O" && a2.atom_site_type_symbol == "O") {
					log_length += "peroxide? bond length: " + length + a1.atom_site_type_symbol + "-" + a2.atom_site_type_symbol + "?";
					//if (length < 1.1) {
						bondorder = 0;
					//} else if (length < 1.15) {
					//	bondorder = 2;
					//} else if (length >= 1.25) {
					//	bondorder = 1;
					//}
				} else if ((a1.atom_site_type_symbol == "N" && a2.atom_site_type_symbol == "O") || (a1.atom_site_type_symbol == "O" && a2.atom_site_type_symbol == "N")) {
					if (length < 1.2) {
						bondorder = 2;
					} else if (length >= 1.2) {
						bondorder = 1;
					}
				} else if (a1.atom_site_type_symbol == "H" && a2.atom_site_type_symbol == "H") {
					log_length += "H-H?";
					log_length += "H-H? bond length: " + length + a1.atom_site_type_symbol + "-" + a2.atom_site_type_symbol + "?";
					bondorder = 0;
				} else if ((a1.atom_site_type_symbol == "H" && a2.atom_site_type_symbol == "O") || (a1.atom_site_type_symbol == "O" && a2.atom_site_type_symbol == "H")) {
					if (length < 1.2) {
						bondorder = 1;
					}
				} else if ((a1.atom_site_type_symbol == "H" && a2.atom_site_type_symbol == "N") || (a1.atom_site_type_symbol == "N" && a2.atom_site_type_symbol == "H")) {
					if (length < 1.2) {
						bondorder = 1;
					}
				}else if ((a1.atom_site_type_symbol == "H" && a2.atom_site_type_symbol == "C") || (a1.atom_site_type_symbol == "C" && a2.atom_site_type_symbol == "H")) {
					if (length < 1.2) {
						bondorder = 1;
					}
				} else {
					//log_length += "atoms? bond length: " + length + a1.atom_site_type_symbol + "-" + a2.atom_site_type_symbol + "?";
					bondorder = 1;
				}
				//Console.WriteLine (a1.Symbol + "-" + a2.Symbol + ": " + bondorder + "; " + ln + ", " + vdwvdw);
			}
			log_length += "\n";
			return bondorder;
		}


		public static double VDW(string AtomicSymbol)
		{
			//A Bondi (1964) J Phys Chem 68:441
			// A
			switch (AtomicSymbol) {
			case "H": return 1.20;
			case "He": return 1.40;
			case "Li": return 1.82;
			case "C": return 1.70;
			case "N": return 1.55;
			case "O": return 1.52;
			case "F": return 1.47;
			case "Ne": return 1.54;
			case "Na": return 2.27;
			case "Mg": return 1.73;
			case "Si": return 2.10;
			case "P": return 1.80;
			case "S": return 1.80;
			case "Cl": return 1.75;
			case "Ar": return 1.88;
			case "K": return 2.75;
			case "Ni": return 1.63;
			case "Cu": return 1.40;
			case "Zn": return 1.39;
			case "Ga": return 1.87;
			case "As": return 1.85;
			case "Se": return 1.90; 
			case "Br": return 1.85;
			case "Kr": return 2.02;
			case "Pd": return 1.63;
			case "Ag": return 1.72;
			case "Cd": return 1.58;
			case "In": return 1.93;
			case "Sn": return 2.17;
			case "Te": return 2.06;
			case "I": return 1.98;
			case "Xe": return 2.16;
			case "Pt": return 1.75;
			case "Au": return 1.66;
			case "Hg": return 1.55;
			case "TI": return 1.96;
			case "Pb": return 2.02;
			case "U": return 1.86;
			default: return 2.00;

			}
		}
	}




	public class Model_Entity_Asym{

		public string model_index { get; set; }
		public string entity_index { get; set; }
		public string asym_id { get; set; }

		public List<Atom> atom_list { get; set; }

		public void Addatom(List<Atom> a_atom_list){
			this.atom_list = a_atom_list;
		}
	}

	public class Model_Entity{

		public string model_index { get; set; }
		public string entity_index { get; set; }
		public List<Atom> atom_list { get; set; }

		public void Addatom(List<Atom> a_atom_list){
			this.atom_list = a_atom_list;
		}
	}

	public class Model{

		public string model_index { get; set; }
		public List<Atom> atom_list { get; set; }

		public void Addatom(List<Atom> a_atom_list){
			this.atom_list = a_atom_list;
		}

	}


	public class Mol{
		public List<Atom> Atoms{ get; set; }
		public List<Bond> Bonds{ get; set; }

	}

	/// <summary>
	/// Data Items Describing Atomic Positions http://mmcif.wwpdb.org/docs/tutorials/content/atomic-description.html
	/// </summary>
	public class Atom{
		/// <summary>
		/// Gets or sets the identifier.
		/// </summary>
		/// <value>The identifier.</value>
		public int id {get; set;}
		/// <summary>
		/// Gets or sets the x.
		/// </summary>
		/// <value>The x.</value>
		public double X { get; set; }
		/// <summary>
		/// Gets or sets the y.
		/// </summary>
		/// <value>The y.</value>
		public double Y { get; set; }
		/// <summary>
		/// Gets or sets the z.
		/// </summary>
		/// <value>The z.</value>
		public double Z { get; set; }
		/// <summary>
		/// Atomic Symbol
		/// </summary>
		/// <value>The symbol.</value>
		public string atom_site_type_symbol { get; set; }
		/// <summary>
		/// Gets or sets the position.
		/// </summary>
		/// <value>The position.</value>
		public string atom_site_label_atom_id_Position { get; set; }
		/// <summary>
		/// PDB 3 letter code
		/// </summary>
		/// <value>The mol code.</value>
		public string atom_site_label_comp_id_molCode { get; set; }
		/// <summary>
		/// Gets or sets the bonds.
		/// </summary>
		/// <value>The bonds.</value>
		//public List<Bond> Bonds { get; set; }

		/// <summary>
		/// Gets or sets the aromatic.
		/// </summary>
		/// <value>The aromatic.</value>
		public bool aromatic { get; set; }

		/// <summary>
		/// Gets or sets the hybrid. sp, sp2, sp3
		/// </summary>
		/// <value>The hybrid.</value>
		public string hybrid { get; set; }

		/// <summary>
		/// 1,2,3, ... important value: PDBj </summary>-> GlyTouCan
		/// </summary>
		/// <value>The entity identifier.</value>
		public string atom_site_label_entity_id { get; set; }

		/// <summary>
		/// 2080, ...
		/// </summary>
		/// <value>The auth seq identifier.</value>
		public string atom_site_label_seq_id { get; set; }

		/// <summary>
		/// NAG, BMA, ... This data item is an author defined alternative to the value of _atom_site.label_comp_id. This item holds the PDB 3-letter-code residue names
		/// </summary>
		/// <value>The auth comp identifier.</value>
		public string auth_comp_id { get; set; }

		/// <summary>
		/// SUGAR (8-MER)
		/// </summary>
		/// <value>The pdbx description.</value>
		public string description { get; set; }

		/// <summary>
		/// C1, O1, ,..
		/// </summary>
		/// <value>The atom identifier.</value>
		public string atom_id  { get; set; }

		/// <summary>
		/// NAG, ..
		/// </summary>
		/// <value>The comp identifier.</value>
		public string comp_id { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <value>The seq identifier.</value>
		public string seq_id { get; set; }

		/// <summary>
		/// A, B, ... This data item is reference to item _struct_asym.id defined in category STRUCT_ASYM. This item identifies an instance of particular entity in the deposited coordinate set. For a structure determined by crystallographic method this corresponds to a unique identifier within the cyrstallographic asymmetric unit.
		/// </summary>
		/// <value>The asym identifier.</value>
		public string atom_site_label_asym_id { get; set; }

		/// <summary>
		/// 1,2,3,.. atom_site.id  - A unique identifier for each atom position.
		/// </summary>
		/// <value>The index of the atom.</value>
		public string atom_site_id { get; set; }

		/// <summary>
		/// 1,2,3 : number of model -This data item identifies the model number in an ensemble of coordinate data.

		/// </summary>
		/// <value>The pdbx PD b model number.</value>
		public string pdbx_PDB_model_num  { get; set; }

		/// <summary>
		/// HETATOM -> true, ATOM -> false
		/// </summary>
		/// <value><c>true</c> if het atom; otherwise, <c>false</c>.</value>
		public bool hetAtom { get; set; }

		/// <summary>
		/// The fraction of the atom present at this atom position.
		/// </summary>
		/// <value>The atom site occupancy.</value>
		public double atom_site_occupancy { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="GlycoNAVI.Atom"/> is terminal heteroatom.
		/// </summary>
		/// <value><c>true</c> if is terminal heteroatom; otherwise, <c>false</c>.</value>
		public bool is_terminal_heteroatom { get; set; }


		public double atom_site_B_iso_or_equiv { get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <value></value>
		public string atom_site_label_alt_id { get; set; }


	}

	public class Bond{
		public int id { get; set; }
		public int fromAtomID {get; set;}
		public string fromAtom { get; set; }
		public int toAtomID {get; set;}
		public string toAtom { get; set; }
		public int BondOrder { get; set; }
		public double length { get; set; }
	}

	//Atom A0, Atom A1, Atom B0, Atom B1
	public class Amide{
		public int id { get; set; }
		public int A0 { get; set; }
		public int A1 { get; set; }
		//public int A2 { get; set; }
		public int B0 { get; set; }
		public int B1 { get; set; }
		public int B2 { get; set; }
	}

}

