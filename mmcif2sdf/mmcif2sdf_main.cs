﻿// Xamarin Studio on MAcOSX 10.9
// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text; 
using System.Collections.Generic;
using System.Collections;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
using GlycoNAVI;
//using DataCompress;
//using System.IO.Compression;
//using System.IO.Compression.FileSystem;
// convertor from mmcif to sdf
// input: PDB mmcif.gz
namespace mmcif2sdf
{
	class MainClass
	{
		public static void Main (string[] args)
		{

			string datatype = "";
			bool checkamidestructure = true;
			//			datatype = "args";
			//			datatype = "updated";
						datatype = "single";
			//			datatype = "new";
			//datatype = "all";
			//			datatype = "file";
			//			datatype = "data";


			string filename = "20160427_PDBj_retry.txt";

			// use local cif file
			//datatype = "local";
			string filePath = "/Users/yamada/Desktop/BH2016/PDB/entry/2yla.cif";
			Boolean bl_web = true;
			//Console.WriteLine ("web access:\t" + bl_web);


			string ftp_pdbid_string = String.Empty;


			// single pdb id
			if (datatype == "single") {
				ftp_pdbid_string = "1GAH";
				//ftp_pdbid_string = "3W3J";
				//ftp_pdbid_string = "1A14";
				//ftp_pdbid_string = "3BJM";
				//ftp_pdbid_string = "1AA5"; // valence error
				ftp_pdbid_string = "1A3K,3KEH,5HMG,154L";
				//ftp_pdbid_string = "3KEH";
				//ftp_pdbid_string = "146D";
				//ftp_pdbid_string = "5FJJ";
				//ftp_pdbid_string = "1CKL";
				//ftp_pdbid_string = "1A14"; 
				//ftp_pdbid_string = "1OVW";
				//ftp_pdbid_string = "1UOZ";
				//ftp_pdbid_string = "2YLA";
				ftp_pdbid_string = "5IRE";
				ftp_pdbid_string = "1FFR"; // checkOccupancy sample
				ftp_pdbid_string = "5FJJ";
				ftp_pdbid_string = "3BJM"; // missing O1
				ftp_pdbid_string = "1AA5"; // atom_site_occupancy is not 1
				ftp_pdbid_string = "1A14"; // amide structure sample
				ftp_pdbid_string = "1OVW"; // amide structure sample

				//ftp_pdbid_string = "1UOZ"; // thio-sugar structure sample
			}





			var utf8_encodingz_without_BOM = new System.Text.UTF8Encoding ();

			StringBuilder log_sb = new StringBuilder ();
			DateTime dt = DateTime.Now;
			string datetimesting = dt.ToString ("yyyy-MM-dd-HH-mm-ss");

			log_sb.AppendLine ("start: " + datetimesting);




			string ftp_pdbid_list = string.Empty;

			string base_dirctry = "sugar";

			//base_dirctry
			if (!Directory.Exists (base_dirctry)) {
				try {
					Directory.CreateDirectory (base_dirctry);
				} catch (Exception ex) {
					Console.WriteLine (ex.ToString ());
					log_sb.AppendLine (base_dirctry + "\tcreate directry error");
				}
			}

			string base_log_dirctry = "sugar_log";

			//base_dirctry
			if (!Directory.Exists (base_log_dirctry)) {
				try {
					Directory.CreateDirectory (base_log_dirctry);
				} catch (Exception ex) {
					Console.WriteLine (ex.ToString ());
					log_sb.AppendLine (base_log_dirctry + "\tcreate directry error");
				}
			}

			string entity_timesting = dt.ToString ("yyyyMMdd");
			if (!Directory.Exists (base_log_dirctry + Path.DirectorySeparatorChar + entity_timesting)) {
				try {
					Directory.CreateDirectory (base_log_dirctry + Path.DirectorySeparatorChar + entity_timesting);
				} catch (Exception ex) {
					Console.WriteLine (ex.ToString ());
					log_sb.AppendLine (base_log_dirctry + Path.DirectorySeparatorChar + entity_timesting + "\tcreate directry error");
				}
			}







			for (int N = 1; N < Environment.GetCommandLineArgs ().Length; N++) {
				
				try {

					if (Environment.GetCommandLineArgs () [N] == "-type") {
						datatype = Environment.GetCommandLineArgs () [N + 1];
						Console.WriteLine ("type:" + datatype);	
					}
					if (Environment.GetCommandLineArgs () [N] == "-id") {
						
						ftp_pdbid_string = Environment.GetCommandLineArgs () [N + 1];
						Console.WriteLine ("ID:" + ftp_pdbid_string);	
					}
					if (Environment.GetCommandLineArgs () [N] == "-file") {

						filename = Environment.GetCommandLineArgs () [N + 1];
						Console.WriteLine ("ID:" + ftp_pdbid_string);	
					}
					if (Environment.GetCommandLineArgs () [N] == "-sac") {
						checkamidestructure = false;
						Console.WriteLine ("skip amide structure check");
					}


					//  -type local -filePath {filePath}
					if (Environment.GetCommandLineArgs () [N] == "-filePath") {
						bl_web = false;
						filePath = Environment.GetCommandLineArgs () [N + 1];

						Console.WriteLine ("use local data");
					}
					if (Environment.GetCommandLineArgs () [N] == "-web") {
						bl_web = true;
					}


					if (Environment.GetCommandLineArgs () [N] == "-help" || Environment.GetCommandLineArgs () [N] == "-h") {
						Console.WriteLine ("*** mmcif2sdf ***");
						Console.WriteLine ("");
						Console.WriteLine ("version 29 Nov 2017 revision A");
						Console.WriteLine ("");
						Console.WriteLine ("options");
						Console.WriteLine ("\t-type: input data type");
						Console.WriteLine ("\t-web: web access is true");
						Console.WriteLine ("\t-id: PDB ID");
						Console.WriteLine ("\t-file: use local data");
						Console.WriteLine ("\t-filePath: local file path");
						Console.WriteLine ("\t-sac: skip amide structure check");
						Console.WriteLine ("");
						Console.WriteLine ("$mono ./mmcif2sdf.exe -type updated");
						Console.WriteLine ("$mono ./mmcif2sdf.exe -type new");
						Console.WriteLine ("$mono ./mmcif2sdf.exe -type all -sac");
						Console.WriteLine ("$mono ./mmcif2sdf.exe -type single -id 1A3K");
						Console.WriteLine ("$mono ./mmcif2sdf.exe -type single -id '1A3K,3KEH,5HMG,154L'");
						Console.WriteLine ("$mono ./mmcif2sdf.exe -type file -file entry.txt");
						Console.WriteLine ("");
						Console.WriteLine ("Issaku YAMADA");
						Console.WriteLine ("");
						datatype = "help";
						ftp_pdbid_string = string.Empty;
						filename = string.Empty;
						checkamidestructure = false;
						break;
					}
				} catch (Exception ex) {
					Console.WriteLine (ex.ToString ());
					log_sb.AppendLine (ex.ToString ());
				}
			}

			Console.WriteLine ("web access:\t" + bl_web);











			try {



				// latest new pdb id
				if (datatype == "new") {
					ftp_pdbid_list = "ftp://ftp.pdbj.org/XML/pdbmlplus/latest_new_pdbid.txt";
				}
				// latest_updated_pdbid

				if (datatype == "updated") {
					ftp_pdbid_list = "ftp://ftp.pdbj.org/XML/pdbmlplus/latest_updated_pdbid.txt";
				}

				if (datatype == "new" || datatype == "updated") {
					WebRequest id_req = WebRequest.Create (ftp_pdbid_list);
					using (WebResponse id_res = id_req.GetResponse ()) {
						using (Stream id_st = id_res.GetResponseStream ()) {
							using (StreamReader reader = new StreamReader (id_st)) {
								ftp_pdbid_string = reader.ReadToEnd ();
								reader.Close ();
							}
						}
					}
				}

				// all_pdbid
				if (datatype == "all") {
					ftp_pdbid_list = "ftp://ftp.pdbj.org/XML/pdbmlplus/all_pdbid.txt.gz";

					WebRequest id_req = WebRequest.Create (ftp_pdbid_list);
					using (WebResponse id_res = id_req.GetResponse ()) {
						using (Stream id_st = id_res.GetResponseStream ()) {

							GZipStream decompStream // 解凍ストリーム
						= new GZipStream (
								 id_st, // 入力元となるストリームを指定
								 CompressionMode.Decompress); // 解凍（圧縮解除）を指定
						

							using (StreamReader reader = new StreamReader (decompStream)) {
								ftp_pdbid_string = reader.ReadToEnd ();
								reader.Close ();
							}
						}
					}

				}

				if (datatype == "file") {

					if (File.Exists (filename)) {
						using (StreamReader reader = new StreamReader (filename)) {
							ftp_pdbid_string = reader.ReadToEnd ();
							reader.Close ();
						}
					}
				}

				if (datatype == "local") {

					if (File.Exists (filePath)) {

						ftp_pdbid_string = Path.GetFileName(filePath).Replace(".cif","");
					}
				}



			} catch (Exception ex) {
				Console.WriteLine (ex.ToString ());
				log_sb.AppendLine (ex.ToString ());
			}








			string[] pdbid_list = ftp_pdbid_string.Split (new []{ "\r\n", "\n", ",", System.Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);




//			pdbid_list = new string[] { pdb4code };


			int count = 1;
			string pdb4code = String.Empty; // output code data

			for (int i = 0; i < pdbid_list.Length; i++) {
				//			foreach (var pdbid in pdbid_list) {

				log_sb = new StringBuilder ();

				if (pdbid_list [i].Length == 4) {
					pdb4code = pdbid_list [i].ToUpper ();

					Console.WriteLine (count + "/" + pdbid_list.Length + "\t" + pdb4code);
					count++;


					log_sb.AppendLine ("#: " + pdb4code);

					try {
						List<string> mmcifString = new List<string> ();


						if (bl_web == true) {

							string uri = "http://pdbj.org/rest/downloadPDBfile?format=mmcif&id=" + pdb4code;

							WebRequest req = WebRequest.Create (uri);

							using (WebResponse res = req.GetResponse ()) {
								using (Stream st = res.GetResponseStream ()) {

									GZipStream decompStream // 解凍ストリーム
									= new GZipStream (
										  st, // 入力元となるストリームを指定
										  CompressionMode.Decompress); // 解凍（圧縮解除）を指定

									string hatom = String.Empty;

									using (StreamReader reader = new StreamReader (decompStream)) {
										while ((hatom = reader.ReadLine ()) != null) {
											mmcifString.Add (hatom);
										}
										reader.Close ();
									}
								}

							}
							System.Diagnostics.Debug.WriteLine ("get responce");
						} else {
							if (File.Exists (filePath)) {


								if (filePath.ToLower ().EndsWith (".cif", StringComparison.OrdinalIgnoreCase)) {
									using (StreamReader reader = new StreamReader (filePath)) {
										string t_line = string.Empty;
										while ((t_line = reader.ReadLine ()) != null) {
											mmcifString.Add (t_line);
										}
									}
								}
								System.Diagnostics.Debug.WriteLine ("get local data");
							}
						}


						// check SUGAR entity
						//						bool nmr_bool = false;
						pdb obj_pdb = new pdb ();

						//string pdbx_seq_one_letter_code_can = string.Empty;
						//bool bool_pdbx_seq_one_letter_code_can = false;

						List<string> SUGAR_Entity_ids = new List<string> ();

						//						1  polymer     man 'PROTEIN (CD46)' 14517.652 6  ? ? 'N-TERMINAL TWO SCR DOMAINS' ? 
						//						2  polymer     man 'SUGAR (8-MER)'  1397.256  2  ? ? ?                            ? 
						for (int j = 0; j < mmcifString.Count; j++) {
							//foreach (var line in mmcifString) {

							//行毎に　”SUGAR”を確認

							try {
								//if (mmcifString [j].Length > 30) {
								if (mmcifString [j].Contains ("'SUGAR (")) {
									try {
										string entity_id = mmcifString [j].Substring (0, 2).Replace (" ", "");
										SUGAR_Entity_ids.Add (entity_id);
									} catch (Exception ex) {
										Console.WriteLine (ex.ToString ());
									}
									System.Diagnostics.Debug.WriteLine ("get SUGAR");
									System.Diagnostics.Debug.WriteLine ("\t" + mmcifString [j]);
								}
							} catch (Exception ex) {
								log_sb.AppendLine (pdb4code + "'SUGAR (" + " checking....");
								log_sb.AppendLine (ex.ToString ());
								Console.WriteLine (pdb4code + "'SUGAR (" + " checking....");
								Console.WriteLine (ex.ToString ());
							}


							try {
								if (obj_pdb.pdbx_gene_src_scientific_name == null && mmcifString [j].Contains ("pdbx_gene_src_scientific_name")) {
									if (mmcifString [j].Length > 51) {
										obj_pdb.pdbx_gene_src_scientific_name = mmcifString [j].Substring (51);
									}
								}
								if (obj_pdb.pdbx_gene_src_ncbi_taxonomy_id == null && mmcifString [j].Contains ("pdbx_gene_src_ncbi_taxonomy_id")) {
									if (mmcifString [j].Length > 51) {
										obj_pdb.pdbx_gene_src_ncbi_taxonomy_id = mmcifString [j].Substring (51);
									}
								}
								if (obj_pdb.pdbx_host_org_scientific_name == null && mmcifString [j].Contains ("pdbx_host_org_scientific_name")) {
									if (mmcifString [j].Length > 51) {
										obj_pdb.pdbx_host_org_scientific_name = mmcifString [j].Substring (51);
									}
								}
								if (obj_pdb.pdbx_host_org_ncbi_taxonomy_id == null && mmcifString [j].Contains ("pdbx_host_org_ncbi_taxonomy_id")) {
									if (mmcifString [j].Length > 51) {
										obj_pdb.pdbx_host_org_ncbi_taxonomy_id = mmcifString [j].Substring (51);
									}
								}
								if (obj_pdb.struct_ref_pdbx_db_accession == null && mmcifString [j].Contains ("_struct_ref.pdbx_db_accession")) {
									if (mmcifString [j].Length > 34) {
										obj_pdb.struct_ref_pdbx_db_accession = mmcifString [j].ToString ().Substring (34).Trim (); //P04062
									}
								}
								if (obj_pdb.reflns_d_resolution_high == null && mmcifString [j].Contains ("_reflns.d_resolution_high")) {
									if (mmcifString [j].Length > 37) {
										obj_pdb.reflns_d_resolution_high = mmcifString [j].Substring (37); //P04062
									}
								}
								if (obj_pdb.struct_ref_db_name == null && mmcifString [j].Contains ("_struct_ref.db_name")) {
									if (mmcifString [j].Length > 37) {
										obj_pdb.struct_ref_db_name = mmcifString [j].Substring (37); //P04062
									}
								}

								if (obj_pdb.exptl_method == null && mmcifString [j].Contains ("_exptl.method")) {
									if (mmcifString [j].Length > 25) {
										obj_pdb.exptl_method = mmcifString [j].Substring (25);
									}
								}

								//if (obj_pdb.pdbx_seq_one_letter_code_can == null && mmcifString [j].Contains ("_entity_poly.pdbx_seq_one_letter_code_can")) {

								//		bool_pdbx_seq_one_letter_code_can = true;

								//}
								//if (mmcifString [j] == ";") {
								//	bool_pdbx_seq_one_letter_code_can = false;
								//}
								//if (bool_pdbx_seq_one_letter_code_can == true && !mmcifString [j].Contains ("_entity_poly.pdbx_seq_one_letter_code_can")) {
								//	pdbx_seq_one_letter_code_can += mmcifString [j]; //.Replace(";","").Trim();
								//}

								// _struct_ref.db_name 

							} catch (Exception ex) {
								log_sb.AppendLine (pdb4code + "get pdb metadata");
								Console.WriteLine (pdb4code + "get pdb metadata");
								Console.WriteLine (ex.ToString ());
							}



						}

						//obj_pdb.pdbx_seq_one_letter_code_can = pdbx_seq_one_letter_code_can.Substring(1);


						// loop_
						//  [0] _pdbx_struct_mod_residue.id 
						//  [1] _pdbx_struct_mod_residue.label_asym_id 
						//  [2] _pdbx_struct_mod_residue.label_seq_id 
						//  [3] _pdbx_struct_mod_residue.label_comp_id 
						//  [4] _pdbx_struct_mod_residue.auth_asym_id 
						//  [5] _pdbx_struct_mod_residue.auth_seq_id 
						//  [6] _pdbx_struct_mod_residue.auth_comp_id 
						//  [7] _pdbx_struct_mod_residue.PDB_ins_code 
						//  [8] _pdbx_struct_mod_residue.parent_comp_id 
						//  [9] _pdbx_struct_mod_residue.details 
						//   1  A 49 ASN A 49 ASN ? ASN 'GLYCOSYLATION SITE' 

						List<GLYCOSYLATION_SITE> glysite_list = new List<GLYCOSYLATION_SITE> ();
						List<string> GLYCOSYLATION_SITES_seq_id = new List<string> ();
						List<string> GLYCOSYLATION_SITES_comp_id = new List<string> ();

						for (int j = 0; j < mmcifString.Count; j++) {
							//foreach (var line in mmcifString) {
							//行毎に　”SUGAR”を確認
							if (mmcifString [j].Contains ("GLYCOSYLATION SITE")) {
								try {

									string seq_id = mmcifString [j].Substring (4, 3).Replace (" ", "");
									if (!GLYCOSYLATION_SITES_seq_id.Contains (seq_id)) {
										GLYCOSYLATION_SITES_seq_id.Add (seq_id);
									}

									string comp_id = mmcifString [j].Substring (7, 4).Replace (" ", "");
									if (!GLYCOSYLATION_SITES_comp_id.Contains (comp_id)) {
										GLYCOSYLATION_SITES_comp_id.Add (comp_id);
									}



									string glycosylsite = mmcifString [j];

									glycosylsite = glycosylsite.Replace ("    ", " ");
									glycosylsite = glycosylsite.Replace ("   ", " ");
									glycosylsite = glycosylsite.Replace ("  ", " ");
									string [] delimiter = { " " };
									string [] elements = glycosylsite.Split (delimiter, StringSplitOptions.None);

									GLYCOSYLATION_SITE glysite = new GLYCOSYLATION_SITE ();

									if (elements.Length > 4) {
										glysite.pdbx_struct_mod_residue_id = elements [0];
										glysite.pdbx_struct_mod_residue_label_asym_id = elements [1];
										glysite.pdbx_struct_mod_residue_label_seq_id = elements [2];
										glysite.pdbx_struct_mod_residue_label_comp_id = elements [3];
									}

									glysite_list.Add (glysite);
									System.Diagnostics.Debug.WriteLine ("fin -- glycosylation site");
								} catch (Exception ex) {
									log_sb.AppendLine (pdb4code + "get GLYCOSYLATION SITE");
									Console.WriteLine (pdb4code + "get GLYCOSYLATION SITE");
									Console.WriteLine (ex.ToString ());
								}


							}
						}

						obj_pdb.GLYCOSYLATION_SITE_LIST = glysite_list;



						// pdbx_PDB_model_num

						//0         1         2         3         4         5         6         7         8         9  
						//0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
						//mmcif
						//HETATM 3685 C  C1  . NAG C 2 .   ? 61.544 4.886  39.807  1.00 59.73 ? ? ? ? ? ? 801 NAG A C1  1 
						//HETATM 6757 O  O   . HOH EA 10 .   ? 58.963  10.572  18.813  1.00 60.45  ? ? ? ? ? ? 3042 HOH C O   1 
						//  [0] _atom_site.group_PDB 
						//  [1] _atom_site.id 
						//  [2] _atom_site.type_symbol 
						//  [3] _atom_site.label_atom_id 
						//  [4] _atom_site.label_alt_id 
						//  [5] _atom_site.label_comp_id 
						//  [6] _atom_site.label_asym_id 
						//  [7] _atom_site.label_entity_id    <-------
						//  [8] _atom_site.label_seq_id 
						//  [9] _atom_site.pdbx_PDB_ins_code 
						// [10] _atom_site.Cartn_x 
						// [11] _atom_site.Cartn_y 
						// [12] _atom_site.Cartn_z 
						// [13] _atom_site.occupancy 
						// [14] _atom_site.B_iso_or_equiv 
						// [15] _atom_site.Cartn_x_esd 
						// [16] _atom_site.Cartn_y_esd 
						// [17] _atom_site.Cartn_z_esd 
						// [18] _atom_site.occupancy_esd 
						// [19] _atom_site.B_iso_or_equiv_esd 
						// [20] _atom_site.pdbx_formal_charge 
						// [21] _atom_site.auth_seq_id 
						// [22] _atom_site.auth_comp_id 
						// [23] _atom_site.auth_asym_id 
						// [24] _atom_site.auth_atom_id 
						// [25] _atom_site.pdbx_PDB_model_num
						List<string> pdbx_PDB_model_num_list = new List<string> ();
						//						List<string> atom_site_auth_asym_id_list = new List<string> ();

						for (int j = 0; j < mmcifString.Count; j++) {

							string comp_id = mmcifString [j];

							comp_id = comp_id.Replace ("    ", " ");
							comp_id = comp_id.Replace ("   ", " ");
							comp_id = comp_id.Replace ("  ", " ");
							string [] delimiter = { " " };
							string [] elements = comp_id.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);
							if (elements.Length > 25) {
								comp_id = elements [25].Trim ();
								if (comp_id != "?" && !pdbx_PDB_model_num_list.Contains (comp_id)) {
									pdbx_PDB_model_num_list.Add (comp_id);

									//if(!atom_site_auth_asym_id_list.Contains(elements [23].Trim ())){
									//	atom_site_auth_asym_id_list.Add(elements [23].Trim ());
									//}
								}
								//atom_site_auth_asym_id_list.Add(elements [23].Trim ());
							}
						}


						int pdbx_PDB_model_num_list_count = pdbx_PDB_model_num_list.Count;



						// get all atom list
						List<Model> pdb_model = new List<Model> ();
						int all_atom_count = 1;

						List<Atom> mmcif_atoms = new List<Atom> ();

						for (int j = 0; j < mmcifString.Count; j++) {
							//foreach (var line in mmcifString) {
							AtomUtility.Get_mmcif_atoms (mmcifString [j], ref all_atom_count, ref mmcif_atoms); //	, pdbx_PDB_model_num_list);
						}

						//						List<Model> pdb_model = new List<Model>();

						// Separate PDB model from mmcif data
						AtomUtility.SeparateModel (ref log_sb, pdbx_PDB_model_num_list, mmcif_atoms, ref pdb_model);


						// mmcif > pdb_model > sugar > entity_id > asym_id

						// check pdb model data
						for (int modelInt = 0; modelInt < pdb_model.Count; modelInt++) {

							List<Model_Entity> mmcif_pdb_model_sugar_entity = new List<Model_Entity> ();
							//AtomUtility.GetHetAtoms (mmcifString [j], ref atomCount, ref atoms, SUGAR_Enrty_ids, GLYCOSYLATION_SITES_seq_id);

							// TODO: SUGAR_Entity_idsからSugarRediduesに変更する。 // PDB Chemical componentsファイルからSugarRediduesをつくる。
							AtomUtility.GetModelEntityAtoms (ref log_sb, ref mmcif_pdb_model_sugar_entity, pdb_model [modelInt], SUGAR_Entity_ids);

							List<Model_Entity_Asym> mmcif_pdb_model_sugar_entity_asym = new List<Model_Entity_Asym> ();

							for (int entInt = 0; entInt < mmcif_pdb_model_sugar_entity.Count; entInt++) {
								//  mmcif > pdb_model > entity_id > asym_id

								AtomUtility.GetModelEntityAsymAtoms (ref mmcif_pdb_model_sugar_entity_asym, mmcif_pdb_model_sugar_entity [entInt]);
							}





							for (int k = 0; k < mmcif_pdb_model_sugar_entity_asym.Count; k++) {
								//foreach (var asym_atoms in array_asym_atoms) {

								//string asym_id_string = String.Empty;



								// add atom of aglycon at anomeric position
								List<Atom> addatom = new List<Atom> ();

								string str_model = pdb4code
									 + ".model_" + mmcif_pdb_model_sugar_entity_asym [k].model_index
									 + ".entity_" + mmcif_pdb_model_sugar_entity_asym [k].entity_index
									+ ".asym_" + mmcif_pdb_model_sugar_entity_asym [k].asym_id;
								System.Diagnostics.Debug.WriteLine (str_model);
								log_sb.AppendLine (str_model);

								addatom = AtomUtility.AddAglyconAnomericCarbons (ref log_sb, mmcif_pdb_model_sugar_entity_asym [k].atom_list, pdb_model [modelInt].atom_list);



								//Console.WriteLine (model_data);


								// check bond vlenth of anomeric atom
								// add atom

								for (int m = 0; m < addatom.Count; m++) {
									//foreach (var adat in addatom) {
									mmcif_pdb_model_sugar_entity_asym [k].atom_list.Add (addatom [m]);
								}

								mmcif_pdb_model_sugar_entity_asym [k].atom_list = AtomUtility.SetAtomIds (mmcif_pdb_model_sugar_entity_asym [k].atom_list);


								// check atom_site_occupancy
								List<Atom> t_oCheckOccuAtoms = new List<Atom> ();
								t_oCheckOccuAtoms = AtomUtility.checkOccupancy (ref log_sb, mmcif_pdb_model_sugar_entity_asym [k].atom_list);

								System.Diagnostics.Debug.WriteLine ("fin -- atoms");

								List<Bond> bonds = new List<Bond> ();
								string log_setbond = String.Empty;
								bonds = CheckBondLength.SetBonds (t_oCheckOccuAtoms, ref log_setbond);
								System.Diagnostics.Debug.WriteLine ("set bond log:\n" + log_setbond);
								log_sb.AppendLine ("set bond log:\n" + log_setbond);

								//bonds = CheckBondLength.SetBonds (mmcif_pdb_model_sugar_entity_asym [k].atom_list);


								StringBuilder sb = new StringBuilder ();
								StringBuilder mmcif_sb = new StringBuilder ();
								StringBuilder local_error_sb = new StringBuilder ();

								//List<Atom> CheckBondAtoms = new List<Atom> ();
								//CheckBondAtoms = t_oCheckOccuAtoms; //mmcif_pdb_model_sugar_entity_asym [k].atom_list;
								//Chemical.checkBonds (ref CheckBondAtoms, ref bonds);
								//t_oCheckOccuAtoms
								string log_bondscheck = String.Empty;
								Chemical.checkBonds (ref t_oCheckOccuAtoms, ref bonds, ref log_bondscheck);
								System.Diagnostics.Debug.WriteLine ("check bond log:\n" + log_bondscheck);
								log_sb.AppendLine ("check bond log:\n" + log_bondscheck);

								Chemical.checkCOCNbond (ref log_sb, ref local_error_sb, ref t_oCheckOccuAtoms, ref bonds, pdb4code);

								System.Diagnostics.Debug.WriteLine ("fin -- bonds");

								bool bl_Valencies = true;

								// debug option
								//checkamidestructure = false;

								if (checkamidestructure == true) {


									Chemical.checkAcetamideStructure (ref log_sb, ref local_error_sb, ref t_oCheckOccuAtoms, ref bonds, pdb4code);
									bl_Valencies = Chemical.checkValencies (ref log_sb, ref local_error_sb, t_oCheckOccuAtoms, bonds);
									System.Diagnostics.Debug.WriteLine ("check amide strcuture check:\t" + pdb4code);
								} else {
									log_sb.AppendLine ("skip amide strcuture check:\t" + pdb4code);
								}




								if (SUGAR_Entity_ids.Contains (mmcif_pdb_model_sugar_entity_asym [k].entity_index)) {

									//if (bl_Valencies) {

									GlycoNAVI.Format.SDFile (ref log_sb, ref sb, t_oCheckOccuAtoms, bonds, pdb4code, mmcif_pdb_model_sugar_entity_asym [k], checkamidestructure, obj_pdb);

									GlycoNAVI.Format.mmcif (ref log_sb, ref mmcif_sb, t_oCheckOccuAtoms, mmcifString);
									//}

									//Console.Write(mmcif_sb.ToString());


									try {







										string pdb4code_dir = base_dirctry + Path.DirectorySeparatorChar + pdb4code;
										// create folder 
										if (Directory.Exists (pdb4code_dir)) {
											try {
												//Directory.CreateDirectory (pdb4code_dir);

												DirectoryInfo target = new DirectoryInfo (pdb4code_dir);
												//ファイル消す
												foreach (FileInfo file in target.GetFiles ()) {
													//file.Delete();
												}


											} catch (Exception ex) {
												Console.WriteLine (ex.ToString ());
												log_sb.AppendLine (pdb4code_dir + "\tcreate directry error");
											}
										} else if (!Directory.Exists (pdb4code_dir)) {
											try {
												Directory.CreateDirectory (pdb4code_dir);
											} catch (Exception ex) {
												Console.WriteLine (ex.ToString ());
												log_sb.AppendLine (pdb4code_dir + "\tcreate directry error");
											}
										}









										string valencecheckExt = string.Empty;
										if (!bl_Valencies) {
											valencecheckExt = ".txt";
										}

										//StreamWriter writer = new StreamWriter("mol" + Path.DirectorySeparatorChar + outFile + "." + datetimesting + ".mol", false, Encoding.UTF8) ;
										//using (StreamWriter writer = new StreamWriter(outFile + ".entity_" + entity_id + "." + asym_id_string + "." + datetimesting + ".sdf", false, Encoding.UTF8) ){



										using (StreamWriter allwriter = new StreamWriter (
											base_log_dirctry + Path.DirectorySeparatorChar +
											entity_timesting + Path.DirectorySeparatorChar +
											"PDBj_SUGAR_"
											+ entity_timesting
																			+ ".sdf" + valencecheckExt, true, utf8_encodingz_without_BOM)) {

											allwriter.Write (sb.ToString ());
											allwriter.Close ();
										}



										using (StreamWriter writer = new StreamWriter (
																		 base_dirctry + Path.DirectorySeparatorChar + pdb4code + Path.DirectorySeparatorChar +
																		 pdb4code
																		 + ".model_" + mmcif_pdb_model_sugar_entity_asym [k].model_index
																		 + ".entity_" + mmcif_pdb_model_sugar_entity_asym [k].entity_index
																		 + ".asym_" + mmcif_pdb_model_sugar_entity_asym [k].asym_id

																		 + ".sdf" + valencecheckExt, false, utf8_encodingz_without_BOM)) {

											writer.Write (sb.ToString ());
											writer.Close ();
										}

										using (StreamWriter mmcifwriter = new StreamWriter (
																			  base_dirctry + Path.DirectorySeparatorChar + pdb4code + Path.DirectorySeparatorChar +
																			  pdb4code
																			  + ".model_" + mmcif_pdb_model_sugar_entity_asym [k].model_index
																			  + ".entity_" + mmcif_pdb_model_sugar_entity_asym [k].entity_index
																			  + ".asym_" + mmcif_pdb_model_sugar_entity_asym [k].asym_id

																			  + ".cif" + valencecheckExt, false, utf8_encodingz_without_BOM)) {

											mmcifwriter.Write (mmcif_sb.ToString ());
											mmcifwriter.Close ();
										}



										Console.WriteLine ("->" + pdb4code
										+ ".model_" + mmcif_pdb_model_sugar_entity_asym [k].model_index
										+ ".entity_" + mmcif_pdb_model_sugar_entity_asym [k].entity_index
										+ ".asym_" + mmcif_pdb_model_sugar_entity_asym [k].asym_id
										+ " contains SUGAR");


										if (bl_Valencies) {

											log_sb.AppendLine (
												pdb4code
												+ ".model_" + mmcif_pdb_model_sugar_entity_asym [k].model_index
												+ "\tentity_id:" + mmcif_pdb_model_sugar_entity_asym [k].entity_index
												+ "\tasymid:" + mmcif_pdb_model_sugar_entity_asym [k].asym_id
												+ "\tOK");
										} else if (bl_Valencies == false) {

											log_sb.AppendLine (
												pdb4code
												+ ".model_" + mmcif_pdb_model_sugar_entity_asym [k].model_index
												+ "\tentity_id:" + mmcif_pdb_model_sugar_entity_asym [k].entity_index
												+ "\tasymid:" + mmcif_pdb_model_sugar_entity_asym [k].asym_id
												+ "\tValencies error");




											using (StreamWriter valencies_writer = new StreamWriter (
												base_log_dirctry + Path.DirectorySeparatorChar + //pdb4code + Path.DirectorySeparatorChar +
												entity_timesting + Path.DirectorySeparatorChar +
												pdb4code
																					   + ".model_" + mmcif_pdb_model_sugar_entity_asym [k].model_index
												+ ".entity_" + mmcif_pdb_model_sugar_entity_asym [k].entity_index
																					   + ".asym_" + mmcif_pdb_model_sugar_entity_asym [k].asym_id

																					   + ".error_log.txt", false, utf8_encodingz_without_BOM)) {


												local_error_sb.AppendLine (pdb4code);
												local_error_sb.AppendLine (".entity_" + mmcif_pdb_model_sugar_entity_asym [k].entity_index);
												local_error_sb.AppendLine (".asym_" + mmcif_pdb_model_sugar_entity_asym [k].asym_id);

												valencies_writer.Write (local_error_sb.ToString ());
												valencies_writer.Close ();
												Console.WriteLine ("->" + pdb4code + " valencies error");
											}



										}




									} catch (Exception ex) {
										Console.WriteLine (ex.ToString ());
										log_sb.AppendLine (
											pdb4code
											+ ".model_" + mmcif_pdb_model_sugar_entity_asym [k].model_index
											+ "\tentity_id:" + mmcif_pdb_model_sugar_entity_asym [k].entity_index
											+ "\tasymid:" + mmcif_pdb_model_sugar_entity_asym [k].asym_id
											+ "\tStreamWriter error");
									}
								}


							} // array_asym_atoms






							//} // array_atoms









						} // pdb_model




					} catch (Exception ex) {
						log_sb.AppendLine (pdb4code + "\terror");
						log_sb.AppendLine (ex.ToString ());
						Console.WriteLine (ex.ToString ());
					}











				}


				try {


					StreamWriter outwriter = new StreamWriter (
						base_log_dirctry + Path.DirectorySeparatorChar +
						entity_timesting + Path.DirectorySeparatorChar +
						datetimesting + ".log.txt",
						true,  // 上書き （ true = 追加 ）
												 utf8_encodingz_without_BOM);

					outwriter.Write (log_sb.ToString ());
					outwriter.Close ();
					Console.WriteLine ("add logfile");



				} catch (Exception ex) {
					log_sb.AppendLine ("logfile writting" + "\terror");
					log_sb.AppendLine (ex.ToString ());
					Console.WriteLine (ex.ToString ());
				}






			}

			try {

				string last_endtimesting = dt.ToString ("yyyy-MM-dd-HH-mm-ss");
				string strFinish = "finished: " + last_endtimesting;


				StreamWriter outwriter = new StreamWriter (
					base_log_dirctry + Path.DirectorySeparatorChar +
					entity_timesting + Path.DirectorySeparatorChar +
					datetimesting + ".log.txt",
					true,  // 上書き （ true = 追加 ）
											 utf8_encodingz_without_BOM);

				outwriter.Write (strFinish);
				outwriter.Close ();
				Console.WriteLine ("wrote logfile");



				/*
				string compressPath = base_log_dirctry + Path.DirectorySeparatorChar +
					entity_timesting + Path.DirectorySeparatorChar +
															 datetimesting + ".log.txt";
				
				try {
					if (File.Exists (compressPath)) {
						FileInfo fi = new FileInfo (compressPath);

						DataCompress.gz.fileCompress (fi);
						Console.WriteLine ("logfile compress");

						FileIO.DeleteFile (compressPath);
						Console.WriteLine ("logfile delete");
						Console.WriteLine (compressPath);

					} else {
						Console.WriteLine (compressPath);
					}
				} catch (Exception ex) {
					Console.WriteLine (ex.ToString());
				}
				*/




			} catch (Exception ex) {
				//log_sb.AppendLine ("logfile writting" + "\terror");
				//log_sb.AppendLine (ex.ToString ());
				Console.WriteLine (ex.ToString ());
			}
			Console.WriteLine ("finished");
		}







		public static Boolean checkFormat(string data){
			return true;
		}

		public static String selectHatom(string data, string ext){
			return data;
		}
	}
}


