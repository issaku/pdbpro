﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace GlycoNAVI
{
	/// <summary>
	/// metadata
	/// </summary>
	public class pdb
	{
		public string pdbx_gene_src_scientific_name  {get; set;}

		public string pdbx_gene_src_ncbi_taxonomy_id  {get; set;}

		public string pdbx_host_org_scientific_name  {get; set;}

		public string pdbx_host_org_ncbi_taxonomy_id  {get; set;}

		/// <summary>
		/// Gets or sets the pdbx db accession. http://www.uniprot.org/uniprot/P04062
		/// </summary>
		/// <value>The pdbx db accession.</value>
		public string struct_ref_pdbx_db_accession {get; set;} // _struct_ref.pdbx_db_accession

		public string exptl_method {get; set;} // _exptl.method    'X-RAY DIFFRACTION' 

		public string reflns_d_resolution_high {get; set;} // _reflns.d_resolution_high


		public string struct_ref_db_name { get; set; }

		public List<GLYCOSYLATION_SITE> GLYCOSYLATION_SITE_LIST {get; set;}


		/// <summary>
		/// _entity_poly.pdbx_seq_one_letter_code_can
		/// </summary>
		/// <value>The entity poly pdbx seq one letter code can.</value>
		//public string pdbx_seq_one_letter_code_can { get; set; } //_entity_poly.pdbx_seq_one_letter_code_can

	}
		
	/// <summary>
	/// GLYCOSYLATION SITE.
	/// </summary>
	public class GLYCOSYLATION_SITE{
		public string pdbx_struct_mod_residue_id  {get; set;} //_pdbx_struct_mod_residue.id 

		public string pdbx_struct_mod_residue_label_asym_id  {get; set;} //_pdbx_struct_mod_residue.label_asym_id

		public string pdbx_struct_mod_residue_label_seq_id {get; set;} //_pdbx_struct_mod_residue.label_seq_id 

		public string pdbx_struct_mod_residue_label_comp_id  {get; set;} //_pdbx_struct_mod_residue.label_comp_id 

	}
}

