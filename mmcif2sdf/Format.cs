﻿// Xamarin Studio on MAcOSX 10.9
// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections;

namespace GlycoNAVI
{
	public class Format
	{


		public static void mmcif(ref StringBuilder log_sb, ref StringBuilder a_sb, List<Atom> a_atoms, List<string> a_mmciflines){
		
			a_sb.AppendLine ("loop_");
			a_sb.AppendLine ("_atom_site.group_PDB ");
			a_sb.AppendLine ("_atom_site.id ");
			a_sb.AppendLine ("_atom_site.type_symbol ");
			a_sb.AppendLine ("_atom_site.label_atom_id ");
			a_sb.AppendLine ("_atom_site.label_alt_id ");
			a_sb.AppendLine ("_atom_site.label_comp_id ");
			a_sb.AppendLine ("_atom_site.label_asym_id ");
			a_sb.AppendLine ("_atom_site.label_entity_id ");
			a_sb.AppendLine ("_atom_site.label_seq_id ");
			a_sb.AppendLine ("_atom_site.pdbx_PDB_ins_code ");
			a_sb.AppendLine ("_atom_site.Cartn_x ");
			a_sb.AppendLine ("_atom_site.Cartn_y ");
			a_sb.AppendLine ("_atom_site.Cartn_z ");
			a_sb.AppendLine ("_atom_site.occupancy ");
			a_sb.AppendLine ("_atom_site.B_iso_or_equiv ");
			a_sb.AppendLine ("_atom_site.Cartn_x_esd ");
			a_sb.AppendLine ("_atom_site.Cartn_y_esd ");
			a_sb.AppendLine ("_atom_site.Cartn_z_esd ");
			a_sb.AppendLine ("_atom_site.occupancy_esd ");
			a_sb.AppendLine ("_atom_site.B_iso_or_equiv_esd ");
			a_sb.AppendLine ("_atom_site.pdbx_formal_charge ");
			a_sb.AppendLine ("_atom_site.auth_seq_id ");
			a_sb.AppendLine ("_atom_site.auth_comp_id ");
			a_sb.AppendLine ("_atom_site.auth_asym_id ");
			a_sb.AppendLine ("_atom_site.auth_atom_id ");
			a_sb.AppendLine ("_atom_site.pdbx_PDB_model_num ");

			List<string> temp_string =new List<string>();


			try {

				List<string> ids = new List<string> ();
				for (int i = 0; i < a_atoms.Count; i++) {
					ids.Add (a_atoms [i].atom_site_id);
				}



				for (int i = 0; i < a_mmciflines.Count; i++) {

					if ((a_mmciflines [i].Contains ("ATOM") || a_mmciflines [i].Contains ("HETATM"))
					   //&& a_mmciflines [i].Length > 94
					) {

						a_mmciflines [i] = a_mmciflines [i].Replace ("    ", " ");
						a_mmciflines [i] = a_mmciflines [i].Replace ("   ", " ");
						a_mmciflines [i] = a_mmciflines [i].Replace ("  ", " ");
						string[] delimiter = { " " };
						string[] elements = a_mmciflines [i].Split (delimiter, StringSplitOptions.None);

						if (elements.Length > 1) {
							if (ids.Contains (elements [1])) {
								a_sb.AppendLine (a_mmciflines [i]);
								//Console.WriteLine (a_mmciflines [i]);
								temp_string.Add(a_mmciflines [i]);
							}
						}
					}

					// linkage 
					// _struct_conn.id 

				}
			} catch (Exception ex) {
				Console.WriteLine (ex.ToString ());
				log_sb.AppendLine (ex.ToString ());
			}

			//Console.WriteLine ("temp_string.Count\t" + temp_string.Count);
		
			a_sb.AppendLine ("#");
		
		}


		public static void SDFile(
			ref StringBuilder log_sb, 
			ref StringBuilder sb
			, List<Atom> atoms
			, List<Bond> bonds
			, string a_pdb4code_file
			, Model_Entity_Asym a_Model_Entry_Asym
			, bool a_checkamidestructure
			, pdb a_pdb
		){
			//StringBuilder sb = new StringBuilder ();
			// start write Molfile(V2000)
			DateTime dateT = DateTime.Now;
			var dtsting = dateT.ToString ("MMddyyHHmm");

			sb.AppendLine ("PDB." + a_pdb4code_file.Replace (".cif", "")
			+ "." + a_Model_Entry_Asym.model_index
			+ "." + a_Model_Entry_Asym.entity_index
			+ "." + a_Model_Entry_Asym.asym_id
			);

			sb.AppendLine ("mmcif2sdf" + dtsting + "3D");
			//
			sb.AppendLine ("GlycoNAVI: " + "atom:" + atoms.Count + " bond:" + bonds.Count + " check structure:" + a_checkamidestructure.ToString ());

			//CTFile(V2000)
			//160  0  0  0  0  0  0  0  0  0999 V2000
			// 47 50  0  0  1  0  0  0  0  0999 V2000
			//sb.Append (String.Format ("{0, 3}", atomicSymbols.Count));
			sb.Append (String.Format ("{0, 3}", atoms.Count));
			sb.Append (String.Format ("{0, 3}", bonds.Count));
			sb.AppendLine ("  0  0  0  0  0  0  0  0999 V2000");

			StringBuilder glysb = new StringBuilder ();
			glysb.AppendLine ("loop_");
			glysb.AppendLine ("mol_atom_id");
			glysb.AppendLine ("atom_site_id");
			glysb.AppendLine ("atom_site_label_asym_id");
			glysb.AppendLine ("atom_site_label_seq_id");
			glysb.AppendLine ("atom_site_label_comp_id_molCode");
			glysb.AppendLine ("atom_site_label_atom_id_Position");
			glysb.AppendLine ("atom_site_occupancy");

				
			//atom
			foreach (var at in atoms) {
				sb.Append (String.Format ("{0,10:0.0000}", at.X));
				sb.Append (String.Format ("{0,10:0.0000}", at.Y));
				sb.Append (String.Format ("{0,10:0.0000}", at.Z) + " ");
				string atomsymbol = "*";

				try {
					atomsymbol = GlycoNAVI.Chemical.checkAtomicSymbol (at.atom_site_type_symbol);
				} catch (Exception ex) {
					System.Diagnostics.Debug.WriteLine (ex.ToString ());
					log_sb.AppendLine (ex.ToString ());
				}
				//Console.WriteLine (atomsymbol);
				sb.Append (String.Format ("{0, -2}", atomsymbol));
//				sb.AppendLine ("  0  0  0  0  0  0  0  0  0  0  0  0 // Position:" + at.atom_id_Position + " asym_id:" + at.asym_id + " comp_id:" + at.comp_id_molCode);
				sb.AppendLine ("  0  0  0  0  0  0  0  0  0  0  0  0"); // entry_id:" + at.entity_id);

				glysb.AppendLine (
					String.Format ("{0,3}", at.id)
					+ "\t" + String.Format ("{0,4}", at.atom_site_id)
					+ "\t" + String.Format ("{0,4}", at.atom_site_label_asym_id)
					+ "\t" + String.Format ("{0,4}", at.atom_site_label_seq_id)
					+ "\t" + String.Format ("{0,4}", at.atom_site_label_comp_id_molCode)
					+ "\t" + String.Format ("{0,-4}", at.atom_site_label_atom_id_Position)
					+ "\t" + at.atom_site_occupancy
				);
			}
			//outsb.AppendLine("wrote atoms in molfile");

			//Bond Line
			for (int j = 0; j < bonds.Count; j++) {
				sb.Append (String.Format ("{0,3}", bonds [j].fromAtomID));
				sb.Append (String.Format ("{0,3}", bonds [j].toAtomID));

				sb.Append (String.Format ("{0,3}", (bonds [j].BondOrder)));
				sb.Append (String.Format ("{0,3}", (0)));
				sb.AppendLine ("  0  0  0");
				//Console.WriteLine(bonds[j].length);
			}
			//outsb.AppendLine("wrote bonds in molfile");

			sb.AppendLine ("M  END                                                                          ");
			sb.AppendLine ("> <PDB_SUGAR_ENTRY>");
			sb.AppendLine (a_pdb4code_file
			+ ".moldel_num:" + a_Model_Entry_Asym.model_index
			+ ".entry_id:" + a_Model_Entry_Asym.entity_index
			+ ".asym_id:" + a_Model_Entry_Asym.asym_id);
			sb.AppendLine ("");
			sb.AppendLine ("> <PDB_CODE>");
			sb.AppendLine (a_pdb4code_file.Replace (".cif", ""));
			sb.AppendLine ("");
			sb.AppendLine ("> <PDB_pdbx_PDB_model_num>");
			sb.AppendLine (a_Model_Entry_Asym.model_index);
			sb.AppendLine ("");
			sb.AppendLine ("> <PDB_label_entity_id>");
			sb.AppendLine (a_Model_Entry_Asym.entity_index);
			sb.AppendLine ("");
			sb.AppendLine ("> <PDB_auth_asym_id>");
			sb.AppendLine (a_Model_Entry_Asym.asym_id);
			sb.AppendLine ("");
			sb.AppendLine ("> <PDB_SUGAR INFO>");
			sb.Append (glysb.ToString ());
			sb.AppendLine ("#");
			sb.AppendLine ("");


			if (a_pdb != null && a_pdb.pdbx_gene_src_scientific_name != null) {
				sb.AppendLine ("> <pdbx_gene_src_scientific_name>");
				sb.AppendLine (a_pdb.pdbx_gene_src_scientific_name);
				sb.AppendLine ("");
			}

			if (a_pdb != null && a_pdb.pdbx_gene_src_ncbi_taxonomy_id != null) {
				sb.AppendLine ("> <pdbx_gene_src_ncbi_taxonomy_id>");
				sb.AppendLine (a_pdb.pdbx_gene_src_ncbi_taxonomy_id);
				sb.AppendLine ("");
			}

			if (a_pdb != null && a_pdb.pdbx_host_org_scientific_name != null) {
				sb.AppendLine ("> <pdbx_host_org_scientific_name>");
				sb.AppendLine (a_pdb.pdbx_host_org_scientific_name);
				sb.AppendLine ("");
			}

			if (a_pdb != null && a_pdb.pdbx_host_org_ncbi_taxonomy_id != null) {
				sb.AppendLine ("> <pdbx_host_org_ncbi_taxonomy_id>");
				sb.AppendLine (a_pdb.pdbx_host_org_ncbi_taxonomy_id);
				sb.AppendLine ("");
			}

			if (a_pdb != null && a_pdb.struct_ref_db_name != null) {
				sb.AppendLine ("> <struct_ref_db_name>");
				sb.AppendLine (a_pdb.struct_ref_db_name.Trim());
				sb.AppendLine ("");
			}

			if (a_pdb != null && a_pdb.struct_ref_pdbx_db_accession != null) {
				sb.AppendLine ("> <struct_ref_pdbx_db_accession>");
				sb.AppendLine (a_pdb.struct_ref_pdbx_db_accession.Trim());
				sb.AppendLine ("");
			}

			if (a_pdb != null && a_pdb.exptl_method != null) {
				sb.AppendLine ("> <exptl_method>");
				sb.AppendLine (a_pdb.exptl_method);
				sb.AppendLine ("");
			}

			if (a_pdb != null && a_pdb.reflns_d_resolution_high != null) {
				sb.AppendLine ("> <reflns_d_resolution_high>");
				sb.AppendLine (a_pdb.reflns_d_resolution_high);
				sb.AppendLine ("");
			}

			// pdbx_seq_one_letter_code_can
			//if (a_pdb != null && a_pdb.pdbx_seq_one_letter_code_can != null) {
			//	sb.AppendLine ("> <pdbx_seq_one_letter_code_can>");
			//	sb.AppendLine (a_pdb.pdbx_seq_one_letter_code_can);
			//	sb.AppendLine ("");
			//}



			if (a_pdb != null && a_pdb.GLYCOSYLATION_SITE_LIST != null && a_pdb.GLYCOSYLATION_SITE_LIST.Count > 0) {
				sb.AppendLine ("> <GLYCOSYLATION_SITE>");
				sb.AppendLine ("loop_");
				sb.AppendLine ("pdbx_struct_mod_residue_id");
				sb.AppendLine ("pdbx_struct_mod_residue_label_asym_id");
				sb.AppendLine ("pdbx_struct_mod_residue_label_seq_id");
				sb.AppendLine ("pdbx_struct_mod_residue_label_comp_id");

				for (int i = 0; i < a_pdb.GLYCOSYLATION_SITE_LIST.Count; i++) {
					sb.Append (String.Format ("{0,4}", a_pdb.GLYCOSYLATION_SITE_LIST [i].pdbx_struct_mod_residue_id));
					sb.Append ("\t" + String.Format ("{0,3}", a_pdb.GLYCOSYLATION_SITE_LIST [i].pdbx_struct_mod_residue_label_asym_id));
					sb.Append ("\t" + String.Format ("{0,5}", a_pdb.GLYCOSYLATION_SITE_LIST [i].pdbx_struct_mod_residue_label_seq_id));
					sb.AppendLine ("\t" + String.Format ("{0,4}", a_pdb.GLYCOSYLATION_SITE_LIST [i].pdbx_struct_mod_residue_label_comp_id));
				}
				sb.AppendLine ("#");
				sb.AppendLine ("");
			}

			sb.AppendLine ("> <CHECK_AMIDE_STRUCTRE>");
			sb.AppendLine (a_checkamidestructure.ToString());
			sb.AppendLine ("");
			sb.AppendLine ("> <CREATE_DATE>");
			sb.AppendLine (dateT.ToString ("yyyy-MM-dd-HH-mm-ss"));
			sb.AppendLine ("");
			sb.AppendLine ("$$$$");
			// end molfile V2000
			//return sb;
		}
	}
}