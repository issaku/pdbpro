﻿using System;
namespace mmcif2sdf
{
	public class FileIO
	{
		/// -----------------------------------------------------------------------------
		/// <summary>
		///     指定したファイルを削除します。</summary>
		/// <param name="stFilePath">
		///     削除するファイルまでのパス。</param>
		/// -----------------------------------------------------------------------------
		public static void DeleteFile (string stFilePath)
		{
			System.IO.FileInfo cFileInfo = new System.IO.FileInfo (stFilePath);

			// ファイルが存在しているか判断する
			if (cFileInfo.Exists) {
				// 読み取り専用属性がある場合は、読み取り専用属性を解除する
				if ((cFileInfo.Attributes & System.IO.FileAttributes.ReadOnly) == System.IO.FileAttributes.ReadOnly) {
					cFileInfo.Attributes = System.IO.FileAttributes.Normal;
				}

				// ファイルを削除する
				cFileInfo.Delete ();
			}
		}
	}
}

