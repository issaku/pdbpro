﻿// Xamarin Studio on MAcOSX 10.9
// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text; 
using System.Collections.Generic;
using System.Collections;
using System.IO.Compression;
using GlycoNAVI;

namespace GlycoNAVI
{
	public class CheckBondLength
	{
		public static List<Bond> SetBonds (List<Atom> atoms, ref string log)
		{
			List<Bond> bonds = new List<Bond> ();

			int bondid = 1;

			for (int i = 0; i < atoms.Count; i++) {
			//foreach (var a1 in atoms) {
				for (int j = 0; j < atoms.Count; j++) {
				//foreach (var a2 in atoms) {
					if (atoms[i].id > atoms[j].id) {
						double length = 0.0;
						int order = Chemical.bondtype4length (atoms[i], atoms[j], ref length, ref log);
						if (order > 0) {
							Bond bd = new Bond ();
							bd.id =bondid;
							bondid++;
							bd.BondOrder = order;
							bd.fromAtomID = atoms[i].id;
							bd.fromAtom = atoms[i].atom_site_type_symbol;
							bd.toAtomID = atoms[j].id;
							bd.toAtom = atoms[j].atom_site_type_symbol;
							bd.length = length;
							bonds.Add (bd);
						}
					}
				}
			}

			return bonds;
		}
	}
}

