﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Windows;
using System.Windows.Input;

namespace GlycoNAVI
{
	public class Calculation
	{

		//----------------------------
		// Public method
		//----------------------------
		/// <summary>
		/// Inners the product.
		/// </summary>
		/// <returns>The product.</returns>
		/// <param name="o">O.</param>
		/// <param name="a">The alpha component.</param>
		/// <param name="b">The blue component.</param>
		public static double innerProduct(Atom o, Atom a, Atom b){
			double result = 0;
			result += (a.X - o.X) * (b.X - o.X);
			result += (a.Y - o.Y) * (b.Y - o.Y);
			result += (a.Z - o.Z) * (b.Z - o.Z);
			return result;
		}

		public static double innerProduct(double[] a, double[] b){
			int mindim = Math.Min(a.Length, b.Length);
			double result = 0;
			for(int ii=0; ii< mindim; ii++){
				result += a[ii]*b[ii];
			}
			return result;
		}

		public static double innerProduct(double[] o, double[] a, double[] b){
			int mindim = Math.Min(a.Length, b.Length);
			mindim = Math.Min(o.Length, mindim);
			double result = 0;
			for(int ii=0; ii< mindim; ii++){
				result += (a[ii]-o[ii])*(b[ii]-o[ii]);
			}
			return result;
		}




		/* 外積計算 */
		/// <summary>
		/// Outers the product. a->o, b->o
		/// </summary>
		/// <returns>The product.</returns>
		/// <param name="o">O.</param>
		/// <param name="a">The alpha component.</param>
		/// <param name="b">The blue component.</param>
		public static double[] outerProduct(Atom o, Atom a, Atom b) {
			double[] output = new double[3];
			output[0] = (a.Y-o.Y) * (b.Z-o.Z) - (a.Z-o.Z) * (b.Y-o.Y);
			output[1] = (a.Z-o.Z) * (b.X-o.X) - (a.X-o.X) * (b.Z-o.Z);
			output[2] = (a.X-o.X) * (b.Y-o.Y) - (a.Y-o.Y) * (b.X-o.X);

			return output;
		}

		public static double[] outerProduct(double[] a, double[] b) {
			double[] output = new double[3];
			output[0] = a[1]*b[2] - a[2]*b[1];
			output[1] = a[2]*b[0] - a[0]*b[2];
			output[2] = a[0]*b[1] - a[1]*b[0];
			return output;
		}

		public static double[] outerProduct(double[] o, double[] a, double[] b) {
			double[] output = new double[3];
			output[0] = (a[1]-o[1]) * (b[2]-o[2]) - (a[2]-o[2]) * (b[1]-o[1]);
			output[1] = (a[2]-o[2]) * (b[0]-o[0]) - (a[0]-o[0]) * (b[2]-o[2]);
			output[2] = (a[0]-o[0]) * (b[1]-o[1]) - (a[1]-o[1]) * (b[0]-o[0]);
			return output;
		}















		/// <summary>
		/// A1-A2-A3の角度を計算します。マイナス、プラスともにあります。
		/// </summary>
		/// <param name="A1"></param>
		/// <param name="A2"></param>
		/// <param name="A3"></param>
		/// <returns></returns>
		public static double GetBondAngle2D(Atom A1, Atom A2, Atom A3)
		{
			Vector v1 = new Vector();
			Vector v2 = new Vector();
			v1 = new Vector(A2.X - A1.X, A2.Y - A1.Y);
			v2 = new Vector(A2.X - A3.X, A2.Y - A3.Y);
			double kakudo = Vector.AngleBetween(v1, v2);

			return kakudo;
		}

		/// <summary>
		/// Length the specified x1, y1, z1, x2, y2 and z2.
		/// </summary>
		/// <param name="x1">The first x value.</param>
		/// <param name="y1">The first y value.</param>
		/// <param name="z1">The first z value.</param>
		/// <param name="x2">The second x value.</param>
		/// <param name="y2">The second y value.</param>
		/// <param name="z2">The second z value.</param>
		public static double length3D(double x1, double y1, double z1, double x2, double y2, double z2) {
			return Math.Sqrt( (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1) + (z2 - z1) * (z2 - z1) );
		}

		public static double length3D(Atom a1, Atom a2) {
			return Math.Sqrt( (a2.X - a1.X) * (a2.X - a1.X) + (a2.Y - a1.Y) * (a2.Y - a1.Y) + (a2.Z - a1.Z) * (a2.Z - a1.Z) );
		}

		/// <summary>
		///  c1-n-c2-oc
		/// </summary>
		/// <returns>The vec3dinner product.</returns>
		/// <param name="c1">C1</param>
		/// <param name="n">N</param>
		/// <param name="c2">C2</param>
		/// <param name="oc">O or C</param>
		public static double amidVec3dinnerProduct(Atom c1, Atom n, Atom c2, Atom oc) {
			return (((n.X - c1.X) * (c2.X - oc.X)) 
				+ ((n.Y - c1.Y) * (c2.Y - oc.Y))
				+ ((n.Z - c1.Z) * (c2.Z - oc.Z)));
			//  1 に近いほど平行である
			//  0 に近いほど直角である
			// -1 に近いほど反対方向で平行である
		}
	}
}

