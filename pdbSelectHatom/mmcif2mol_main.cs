﻿// Xamarin Studio on MAcOSX 10.9
// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text; 
using System.Collections.Generic;
using System.Collections;
using System.IO.Compression;
using GlycoNAVI;

// convertor from mmcif to mol
// input: PDB mmcif.gz
namespace mmcif2mol
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			StringBuilder outsb = new StringBuilder ();
			string file_tmp = String.Empty;
			string dirname = String.Empty;
			DateTime dt = DateTime.Now;
			string datetimesting = dt.ToString ("MMddyyHHmm");
			outsb.AppendLine ("start: " + datetimesting);

			for (int N = 1; N < Environment.GetCommandLineArgs ().Length; N++) {

				file_tmp = Environment.GetCommandLineArgs () [N];

				dirname = Path.GetDirectoryName(file_tmp);
				string[] files = System.IO.Directory.GetFiles(dirname, "*.gz", System.IO.SearchOption.AllDirectories);

				foreach (var inFile in files) {
					if (File.Exists (inFile)) {

						outsb.AppendLine ("#: " + inFile);
						// 入力ファイルは.gzファイルのみ有効
						if (!inFile.ToLower().EndsWith(".gz")) {
							outsb.AppendLine (inFile + "\tcheck ext (gz?)");
							return;
						}

						try {
							StringBuilder sb = new StringBuilder ();
							// ファイル名末尾の「.gz」を削除
							string outFile = inFile.Substring(0, inFile.Length - 3);

							FileStream inStream // 入力ストリーム
							= new FileStream(inFile, FileMode.Open, FileAccess.Read);

							GZipStream decompStream // 解凍ストリーム
							= new GZipStream(
								inStream, // 入力元となるストリームを指定
								CompressionMode.Decompress); // 解凍（圧縮解除）を指定
															
							string hatom = String.Empty;

							//sb.AppendLine ("GlycoNAVI: " + inFile);
							using (StreamReader reader = new StreamReader(decompStream)) {
							
								List<Atom> atoms = new List<Atom>();

								//行毎に　”HETATM”を確認
								int atomCount = 1;


								while ((hatom = reader.ReadLine ()) != null) {

									// アミノ酸種類ごとに処理を行うのではなく、アミノ酸側鎖のヘテロアトムを抽出する。＆
									// C-Glycan (C-マンノシル化)への対応を行うほうが現実的かも？

									//N-Glycan
									if (hatom.Contains ("ATOM") 
										&& (hatom.Contains(". ASN") || hatom.Contains(". ARG")) //http://onlinelibrary.wiley.com/doi/10.1002/anie.201407824/abstract
										&& hatom.Contains(" N ")                                //アルギニン結合Ｎ-グリカン
										&& hatom.Length > 94) {

										Atom at = new Atom ();
										at.id = atomCount;
										atomCount++;

										hatom = hatom.Replace ("    ", " ");
										hatom = hatom.Replace ("   ", " ");
										hatom = hatom.Replace ("  ", " ");
										string[] delimiter = { " " };
										string[] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

										at.X = double.Parse(elements[10]);
										at.Y = double.Parse(elements[11]);
										at.Z = double.Parse(elements[12]);								
										at.Symbol   = elements [2];
										at.Position = elements [3];
										at.molCode  = elements [5];
										atoms.Add (at);

									}
									//O-Glycan HYP:(4R)-4-hydroxy-L-proline, 5-HYDROXYLYSINE (LYZ)
									// http://www.chem.qmul.ac.uk/iupac/misc/glycp.html
									else if (hatom.Contains ("ATOM") 
										&& (hatom.Contains(". THR") || hatom.Contains(". SER") || hatom.Contains(". HYP") || hatom.Contains(". LYZ")) 
										&& hatom.Contains(" O ")
										&& hatom.Length > 94) {

										Atom at = new Atom ();
										at.id = atomCount;
										atomCount++;

										hatom = hatom.Replace ("    ", " ");
										hatom = hatom.Replace ("   ", " ");
										hatom = hatom.Replace ("  ", " ");
										string[] delimiter = { " " };
										string[] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

										at.X = double.Parse(elements[10]);
										at.Y = double.Parse(elements[11]);
										at.Z = double.Parse(elements[12]);												
										at.Symbol   = elements [2];
										at.Position = elements [3];
										at.molCode  = elements [5];
										;
										atoms.Add (at);

									}
									// C-Glycan
									else if (hatom.Contains ("ATOM")
										&& hatom.Contains(". TRP") // C-マンノシル化
										&& hatom.Contains(" C ")
										&& hatom.Length > 94) {

										Atom at = new Atom ();
										at.id = atomCount;
										atomCount++;

										hatom = hatom.Replace ("    ", " ");
										hatom = hatom.Replace ("   ", " ");
										hatom = hatom.Replace ("  ", " ");
										string[] delimiter = { " " };
										string[] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

										at.X = double.Parse(elements[10]);
										at.Y = double.Parse(elements[11]);
										at.Z = double.Parse(elements[12]);												
										at.Symbol   = elements [2];
										at.Position = elements [3];
										at.molCode  = elements [5];
										atoms.Add (at);

									}
									else if (hatom.Contains ("HETATM") 
										&& !hatom.Contains(" HOH ") 
										&& hatom.Length > 94) {
										//0         1         2         3         4         5         6         7         8         9  
										//0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
										//mmcif
										//HETATM 3685 C  C1  . NAG C 2 .   ? 61.544 4.886  39.807  1.00 59.73 ? ? ? ? ? ? 801 NAG A C1  1 

										//61.544     4.886  .544 4.8  C  0  0  0  0  0  0  0  0  0  0  0  0

										if (hatom.Substring (12, 2).Trim ().ToUpper() != "H") {

											Atom at = new Atom ();
											at.id = atomCount;
											atomCount++;

											hatom = hatom.Replace ("    ", " ");
											hatom = hatom.Replace ("   ", " ");
											hatom = hatom.Replace ("  ", " ");
											string[] delimiter = { " " };
											string[] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

											at.X = double.Parse(elements[10]);
											at.Y = double.Parse(elements[11]);
											at.Z = double.Parse(elements[12]);								
											at.Symbol   = elements [2];
											at.Position = elements [3];
											at.molCode  = elements [5];
											atoms.Add (at);
										}
									}
								}
								//Console.WriteLine("get atom information from mmcif");
								//outsb.AppendLine ("get atom information from mmcif");

								//double vdw = GlycoNAVI.chemical.VDW ("H");
								List<Bond> bonds = new List<Bond> ();
								//bond-order
								int bondid = 1;
								foreach (var a1 in atoms) {
									foreach (var a2 in atoms) {
										if (a1.id > a2.id) {
											double length = 0.0;
											int order = chemical.bondtype4length (a1, a2, ref length);
											if (order > 0) {
												Bond bd = new Bond ();
												bd.id =bondid;
												bondid++;
												bd.BondType = order;
												bd.fromAtomID = a1.id;
												bd.fromAtom = a1.Symbol;
												bd.toAtomID = a2.id;
												bd.toAtom = a2.Symbol;
												bd.length = length;
												bonds.Add (bd);
											}
										}
									}
								}
								//Console.WriteLine("check bond order");
								//outsb.AppendLine ("check bond order");
								//原子に結合している結合を格納
								List<int> bondingAtoms = new List<int>();
								for (int j = 0; j < bonds.Count; j++) {
									if (!bondingAtoms.Contains(bonds[j].fromAtomID)){
										bondingAtoms.Add(bonds[j].fromAtomID);
									}
									if (!bondingAtoms.Contains(bonds[j].toAtomID)){
										bondingAtoms.Add(bonds[j].toAtomID);
									}
								}
								//Console.WriteLine("set bond to atom");
								//outsb.AppendLine ("set bond to atom");

								//結合のある原子のみを抽出
								List<Atom> atomdata = new List<Atom> ();
								//Console.WriteLine("start bonding atom check: " + atoms.Count);
								//outsb.AppendLine ("start bonding atom check: " + atoms.Count);

								int atomNumber = 1;
								for (int i = 0; i < atoms.Count; i++) {

									//Console.WriteLine("bond count: " + atoms[i].Bonds.Count);

									if (bondingAtoms.Contains(atoms[i].id)) {
										Atom at = new Atom();
										at.X = atoms[i].X;
										at.Y = atoms[i].Y;
										at.Z = atoms[i].Z;
										at.Symbol = atoms[i].Symbol;
										at.Position = atoms[i].Position;
										at.molCode = atoms[i].molCode;
										at.id = atomNumber;
										atomNumber++;
										atomdata.Add (at);
									}
								}
								//Console.WriteLine("choose bonding atom");
								//outsb.AppendLine("choose bonding atom");

								//結合情報を再度確認
								bonds = new List<Bond> ();
								//bond-order
								bondid = 1;
								foreach (var a1 in atomdata) {
									foreach (var a2 in atomdata) {
										if (a1.id < a2.id) {
											double length = 0.0;
											int order = chemical.bondtype4length (a1, a2, ref length);
											if (order > 0) {
												Bond bd = new Bond ();
												bd.id = bondid;
												bondid++;
												bd.BondType = order;
												bd.fromAtomID = a1.id;
												bd.fromAtom = a1.Symbol;
												bd.toAtomID = a2.id;
												bd.toAtom = a2.Symbol;
												bd.length = length;
												bonds.Add (bd);
											}
										}
									}
								}

								//Console.WriteLine("check bond length information");
								//outsb.AppendLine("check bond length information");

								//結合角と結合長を考慮した結合次数の決定

								//Bond Listをconnectに格納
								List<List<Bond>> connect = new List<List<Bond>>();

								for (int i = 0; i < atomdata.Count; i++) {
									List<Bond> bd = new List<Bond>();
									for (int j = 0; j < bonds.Count; j++) {
										if (atomdata[i].id == bonds[j].fromAtomID || atomdata[i].id == bonds[j].toAtomID){
											bd.Add(bonds[j]);
										}
									}
									if (bd.Count > 0){
										connect.Add(bd);
									}
								}
									
								//周りの原子との結合の数と結合角から結合を再度検討

								// -NHC(=O)CH3の C-N が C=N となっている。O=C(=NC)C の構造があったら、C-NH-C(=O)-C とする。
								// この際、アミド結合の cis-,trans-を確認し、trans-であれば、OとCの元素記号を交換する
								//
								//      C*			      C*                    C*
								//       \			       \                     \
								//        N				   N                     N-H
								//        || 			   ||                    |
								//        C* - C           C* - O*               C* - C
								//       //               /                     //
								//      O*               C                     O
								//
								//      trans-amide    cis-amide               trans-amide
								//      stable         unstable                stable
								//      C=N (x)        C=N (x)
								//


								int count = 1;
								List<List<int>> amid_CNs = new List<List<int>>();
								List<List<int>> carboxy_COs = new List<List<int>>();
								//Console.WriteLine("connect count: " + connect.Count);
								foreach (var c in connect){
									int connectNumber = 0;
									//string data = string.Empty;
									List<int> ids = new List<int>();
									List<int> carboxy_ids = new List<int>();
									foreach (var b in c){
										//Console.WriteLine(count + " bond: " + b.fromAtomID + "-" + b.toAtomID + " : " + b.BondType);

										/*
										data += count + ">>>>>> bond: " 
											+ b.id + " : "
											+ b.fromAtomID + "(" + b.fromAtom + ")-" 
											+ b.toAtomID + "(" + b.toAtom + ")" + ": " 
											+ b.BondType + "\r\n";
										*/

										//原子周りの結合数の追加
										connectNumber += b.BondType;

										if ((b.fromAtom == "N" && b.toAtom == "C") || (b.fromAtom == "C" && b.toAtom == "N")){
											ids.Add(b.id);
										}
										else if ((b.fromAtom == "O" && b.toAtom == "C") || (b.fromAtom == "C" && b.toAtom == "O")){
											carboxy_ids.Add(b.id);
										}
									}
									count++;
									//Console.WriteLine(count + " connectNumber: " + connectNumber);
									if (connectNumber > 4) {
										//Console.WriteLine(count + " connectNumber: " + connectNumber);
										//Console.WriteLine(data);
										if (ids.Count > 0){
											amid_CNs.Add(ids);
										}
										if (carboxy_COs.Count > 0){
											carboxy_COs.Add(carboxy_ids);
										}
									}

								}

								/*
								count = 1;
								Console.WriteLine(">> amid_CNs");
								foreach (var am in amid_CNs){
									Console.WriteLine("# count: " + count);
									foreach (var a in am){
										Console.WriteLine(a);
									}
								}
								Console.WriteLine("<< amid_CNs");
								*/





//								* A1         B1  A1         *   A1         *
//								*   \       /      \       /      \       /
//								*    A0 - B0        A0 - B0        A0 - B0
//							 	*   /       \      /       \      /       \
//								*  *         *    *         B1   *         *
								//Atom A0, Atom A1, Atom B0, Atom B1, Atom B2 = CH3 of Ac
								//Atom N, Atom C, Atom C*, Atom O*
								List<Amido> amidounit = new List<Amido>();

								count = 1;
								foreach (var c in amid_CNs){
									Amido amd = new Amido();

									foreach (var id_t in c){

										int id = id_t - 1;  //配列が"0"から始まるため

										//Console.WriteLine("id in c: " + id);

										//Console.Write("amid: " + bonds[id].fromAtom + "(" + bonds[id].fromAtomID + ")");
										//Console.WriteLine(" - " + bonds[id].toAtom + "(" + bonds[id].toAtomID + ")");

										if (bonds[id].fromAtom == "N" && bonds[id].toAtom == "C") {
											amd.A0 = bonds[id].fromAtomID;
											amd.B0 = bonds[id].toAtomID;

											foreach (var at in connect[amd.A0 - 1]){
												if (at.fromAtomID == amd.A0 && at.toAtom == "C" && at.toAtomID != amd.B0){
													amd.A1 = at.toAtomID;
													continue;
												}
												else if (at.toAtomID == amd.A0 && at.fromAtom == "C" && at.fromAtomID != amd.B0){
													amd.A1 = at.fromAtomID;
													continue;
												}
											}

											foreach (var at in connect[amd.B0 - 1]){
												if (at.fromAtomID == amd.B0 && at.toAtom == "O"){
													amd.B1 = at.toAtomID;
													continue;
												}
												else if (at.toAtomID == amd.B0 && at.fromAtom == "O"){
													amd.B1 = at.fromAtomID;
													continue;
												}
												else if (at.fromAtomID == amd.B0 && at.toAtom == "C" && at.toAtomID != amd.A0){
													amd.B2 = at.toAtomID;
													continue;
												}
												else if (at.toAtomID == amd.B0 && at.fromAtom == "C" && at.fromAtomID != amd.A0){
													amd.B2 = at.fromAtomID;
													continue;
												}
											}

										}
										else if (bonds[id].fromAtom == "C" && bonds[id].toAtom == "N") {
											amd.B0 = bonds[id].fromAtomID;
											amd.A0 = bonds[id].toAtomID;

											foreach (var at in connect[amd.A0 - 1]){
												if (at.fromAtomID == amd.A0 && at.toAtom == "C" && at.toAtomID != amd.B0){
													amd.A1 = at.toAtomID;
													continue;
												}
												else if (at.toAtomID == amd.A0 && at.fromAtom == "C" && at.fromAtomID != amd.B0){
													amd.A1 = at.fromAtomID;
													continue;
												}
											}

											foreach (var at in connect[amd.B0 - 1]){
												if (at.fromAtomID == amd.B0 && at.toAtom == "O"){
													amd.B1 = at.toAtomID;
													continue;
												}
												else if (at.toAtomID == amd.B0 && at.fromAtom == "O"){
													amd.B1 = at.fromAtomID;
													continue;
												}
												else if (at.fromAtomID == amd.B0 && at.toAtom == "C" && at.toAtomID != amd.A0){
													amd.B2 = at.toAtomID;
													continue;
												}
												else if (at.toAtomID == amd.B0 && at.fromAtom == "C" && at.fromAtomID != amd.A0){
													amd.B2 = at.fromAtomID;
													continue;
												}
											}
										}
									}
									amd.id = count;
									count++;
									amidounit.Add(amd);
								}

								count = 1;
								// trans-アミドならはSynとなる。
								foreach (var am in amidounit){

									Atom A0 = new Atom();
									Atom A1 = new Atom();
									Atom B0 = new Atom();
									Atom B1 = new Atom();
									Atom B2 = new Atom();

									foreach (var at in atomdata){
										if (at.id == am.A0){
											A0 = at;
										}
										else if (at.id == am.A1){
											A1 = at;
										}
										else if (at.id == am.B0){
											B0 = at;
										}
										else if (at.id == am.B1){
											B1 = at;
										}
										else if (at.id == am.B2){
											B2 = at;
										}
									}

									string cistrans = chemical.synanti(A0, A1, B0, B1);

									if (cistrans == "Syn"){
										//Console.WriteLine(count + ": trans-amid");
										count++;
									}
									else if (cistrans == "Anti"){
										//Console.WriteLine("?????" + count + ": cis-amid");

										count++;

										outsb.AppendLine("Please check a geometry of amido group.");


										//NHAcである場合、cis-をtransにする処理を追加する。
										//C と　O の元素記号と結合次数をC(B0)-C & C(B0)=O とする。

										outsb.AppendLine (
											"[A0](" 
											+ String.Format ("{0,10:0.0000}", A0.X) + ","
											+ String.Format ("{0,10:0.0000}", A0.Y) + ","
											+ String.Format ("{0,10:0.0000}", A0.Z) + ") "
											+ A0.Symbol + "[" 
											+ String.Format ("{0,3}", A0.id) + "] Code:" 
											+ A0.molCode + " Position:" 
											+ A0.Position);

										outsb.AppendLine (
											"[A1](" 
											+ String.Format ("{0,10:0.0000}", A1.X) + ","
											+ String.Format ("{0,10:0.0000}", A1.Y) + ","
											+ String.Format ("{0,10:0.0000}", A1.Z) + ") "
											+ A1.Symbol + "[" 
											+ String.Format ("{0,3}", A1.id) + "] Code:" 
											+ A1.molCode + " Position:" 
											+ A1.Position);

										outsb.AppendLine (	
											"[B0](" 
											+ String.Format ("{0,10:0.0000}", B0.X) + ","
											+ String.Format ("{0,10:0.0000}", B0.Y) + ","
											+ String.Format ("{0,10:0.0000}", B0.Z) + ") "
											+ B0.Symbol + "[" 
											+ String.Format ("{0,3}", B0.id) + "] Code:" 
											+ B0.molCode + " Position:" 
											+ B0.Position);

										outsb.AppendLine (
											"[B1](" 
											+ String.Format ("{0,10:0.0000}", B1.X) + ","
											+ String.Format ("{0,10:0.0000}", B1.Y) + ","
											+ String.Format ("{0,10:0.0000}", B1.Z) + ") "
											+ B1.Symbol + "[" 
											+ String.Format ("{0,3}", B1.id) + "] Code:" 
											+ B1.molCode + " Position:" 
											+ B1.Position);

										outsb.AppendLine (
											"[B2](" 
											+ String.Format ("{0,10:0.0000}", B2.X) + ","
											+ String.Format ("{0,10:0.0000}", B2.Y) + ","
											+ String.Format ("{0,10:0.0000}", B2.Z) + ") "
											+ B2.Symbol + "[" 
											+ String.Format ("{0,3}", B2.id) + "] Code:" 
											+ B2.molCode + " Position:" 
											+ B2.Position);


										//B1, B2を座標はそのままで元素記号を入れ替える。

										//Console.WriteLine(">>> B1.Position: " + B1.Position);
										//Console.WriteLine(">>> B2.Position: " + B2.Position);

										for (int i = 0; i < atomdata.Count; i++) {
											if (atomdata[i].id == B1.id){
												//Console.WriteLine("B1 -> B2");
												Atom at = new Atom();
												at.id = B1.id;
												at.X = B1.X;
												at.Y = B1.Y;
												at.Z = B1.Z;
												at.molCode = B2.molCode;
												at.Position = B2.Position;
												at.Symbol = B2.Symbol;
												atomdata[i] = at;

												//Console.WriteLine("   B1->B2: " + B2.Position + ": " + B2.Symbol);
												//Console.WriteLine("at:B1->B2: " + at.Position + ": " + at.Symbol);

											}
											else if (atomdata[i].id == B2.id){
												Atom at = new Atom();
												at.id = B2.id;
												at.X = B2.X;
												at.Y = B2.Y;
												at.Z = B2.Z;
												at.molCode = B1.molCode;
												at.Position = B1.Position;
												at.Symbol = B1.Symbol;
												atomdata[i] = at;

												//Console.WriteLine("   B2->B1: " + B1.Position + ": " + B1.Symbol);
												//Console.WriteLine("at:B1->B2: " + at.Position + ": " + at.Symbol);

											}
										}

										//B0=B1 -> B0-B1; B0-B2 -> B0=B2の結合次数を変更する。
										for (int j = 0; j < bonds.Count; j++) {
											if ((bonds[j].fromAtomID == B0.id && bonds[j].toAtomID == B1.id)
												||
												(bonds[j].fromAtomID == B1.id && bonds[j].toAtomID == B0.id))
											{
												bonds[j].BondType = 1;
											}
											if ((bonds[j].fromAtomID == B0.id && bonds[j].toAtomID == B2.id)
												||
												(bonds[j].fromAtomID == B2.id && bonds[j].toAtomID == B0.id))
											{
												bonds[j].BondType = 2;
											}
										}


										//変更したことをログに残す。
										outsb.AppendLine ("structure changed from cis-amid to trans-amid form.");
										outsb.AppendLine ("Please check amid atoms: " 
											+ A0.molCode + ","
											+ A0.Position + "; "
											+ A1.molCode + ","
											+ A1.Position + "; "
											+ B0.molCode + ","
											+ B0.Position + "; "
											+ B1.molCode + ","
											+ B1.Position + "; "
										);



									}
									else {
										//Console.WriteLine(count + ": ?-amid");
										count++;
									}
















									//* A1         B1  A1         *   A1         *
									//*   \       /      \       /      \       /
									//*    A0 - B0        A0 - B0        A0 - B0
									//*   /       \      /       \      /       \
									//*  *         *    *         B1   *         *
									//Atom A0, Atom A1, Atom B0, Atom B1
									//Atom N, Atom C, Atom C*, Atom O*


									//c1-n-c2-oc
									//(Atom c1, Atom n, Atom c2, Atom oc) 
									//double ip = Calculation.amidVec3dinnerProduct(atomdata[am.A1], atomdata[am.A0], atomdata[am.B0], atomdata[am.B1]);
									//Console.WriteLine("amidVec3dinnerProduct: " + ip);

								}
								//Console.WriteLine("<< end amido unit cis or trans ?");





								//amide C-N bond to single
								for (int j = 0; j < bonds.Count; j++) {
									for (int i = 0; i < amid_CNs.Count; i++) {
										if (amid_CNs[i].Contains(bonds[j].id)){
											bonds[j].BondType = 1;
										}
									}
								}




					

								/*
								foreach (var cs in connect){

								}


								for (int j = 0; j < bonds.Count; j++) {
									for (int i = 0; i < amid_CNs.Count; i++) {
										if (amid_CNs[i].Contains(bonds[j].id)){


										}
									}
								}
								*/





								// carboxyl group -C(=O)=O to -C(=O)O
								//
								List<int> selectCO2 = new List<int>();
								foreach (var c in carboxy_COs){
									//Console.WriteLine("carboxy_CO count: " + c.Count);
									if (c.Count == 2) {
										foreach (var a in c){
											//Console.WriteLine("carboxy_COs: " + a);
											selectCO2.Add(a);
											break;
										}
									}
								}

								/*
								foreach (var c in selectCO2){
									Console.WriteLine("CO2: " + c);
								}
								*/

								//carboxyl C=O bond to single
								for (int j = 0; j < bonds.Count; j++) {
									if (selectCO2.Contains(bonds[j].id)){
										bonds[j].BondType = 1;
									}
								}










								GlycoNAVI.Format.Molfile(ref sb, atomdata, bonds, inFile);


								/*

								// start write Molfile(V2000)
								dt = DateTime.Now;
								datetimesting = dt.ToString ("MMddyyHHmm");

								sb.AppendLine ("GlycoNAVI: " + inFile);

								sb.AppendLine ("mmcif2mol" + datetimesting + "3D");
								//
								sb.AppendLine ("atom:" + atomdata.Count + " bond:" + bonds.Count);

								//CTFile(V2000)
								//160  0  0  0  0  0  0  0  0  0999 V2000
								// 47 50  0  0  1  0  0  0  0  0999 V2000
								//sb.Append (String.Format ("{0, 3}", atomicSymbols.Count));
								sb.Append (String.Format ("{0, 3}", atomdata.Count));
								sb.Append (String.Format ("{0, 3}", bonds.Count));
								sb.AppendLine ("  0  0  0  0  0  0  0  0999 V2000");

								//atom
								foreach (var at in atomdata) {
									sb.Append (String.Format ("{0,10:0.0000}", at.X));
									sb.Append (String.Format ("{0,10:0.0000}", at.Y));
									sb.Append (String.Format ("{0,10:0.0000}", at.Z) + "  ");
									string atomsymbol = GlycoNAVI.chemical.checkAtomicSymbol(at.Symbol);
									//Console.WriteLine (atomsymbol);
									sb.Append (String.Format ("{0, -2}", atomsymbol));
									sb.AppendLine ("  0  0  0  0  0  0  0  0  0  0  0  0");
								}
								//outsb.AppendLine("wrote atoms in molfile");

								//Bond Line
								for (int j = 0; j < bonds.Count; j++)
								{
									sb.Append(String.Format("{0,3}", bonds[j].fromAtomID));
									sb.Append(String.Format("{0,3}", bonds[j].toAtomID));

									sb.Append(String.Format("{0,3}", (bonds[j].BondType)));
									sb.Append(String.Format("{0,3}", (0)));
									sb.AppendLine("  0  0  0");
									//Console.WriteLine(bonds[j].length);
								}
								//outsb.AppendLine("wrote bonds in molfile");

								sb.AppendLine ("M  END                                                                          ");
								// end molfile V2000
								*/

									reader.Close ();
							}
							//outsb.AppendLine("fin molfile");
							//Console.WriteLine(sb.ToString());

							try {
								//StreamWriter writer = new StreamWriter("mol" + Path.DirectorySeparatorChar + outFile + "." + datetimesting + ".mol", false, Encoding.UTF8) ;
								StreamWriter writer = new StreamWriter(outFile + "." + datetimesting + ".mol", false, Encoding.UTF8) ;

								writer.Write(sb.ToString()) ;
								writer.Close() ;
								outsb.AppendLine (inFile + "\tOK");
							}
							catch (Exception ex){
								Console.WriteLine (ex.ToString ());
								outsb.AppendLine (inFile + "\tER");
							}
						}
						catch (Exception ex){
							outsb.AppendLine (inFile + "\tER");
							Console.WriteLine (ex.ToString ());
						}
					}
				}
			}

			string endtimesting = dt.ToString ("MMddyyHHmm");
			outsb.AppendLine ("finished: " + endtimesting);
			StreamWriter outwriter = new StreamWriter(datetimesting + ".log",
				false,  // 上書き （ true = 追加 ）
				Encoding.UTF8) ;

			outwriter.Write(outsb.ToString()) ;
			outwriter.Close() ;

			Console.WriteLine ("finished");
		}
			
		public static Boolean checkFormat(string data){
			return true;
		}

		public static String selectHatom(string data, string ext){
			return data;
		}
	}
}


