﻿// Xamarin Studio on MAcOSX 10.9
// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Collections;

namespace GlycoNAVI
{
	public class Format
	{
		public static void Molfile(ref StringBuilder sb, List<Atom> atoms, List<Bond> bonds, string inFile){
			//StringBuilder sb = new StringBuilder ();
			// start write Molfile(V2000)
			DateTime dateT = DateTime.Now;
			var dtsting = dateT.ToString ("MMddyyHHmm");

			sb.AppendLine ("GlycoNAVI: " + inFile);

			sb.AppendLine ("mmcif2mol" + dtsting + "3D");
			//
			sb.AppendLine ("atom:" + atoms.Count + " bond:" + bonds.Count);

			//CTFile(V2000)
			//160  0  0  0  0  0  0  0  0  0999 V2000
			// 47 50  0  0  1  0  0  0  0  0999 V2000
			//sb.Append (String.Format ("{0, 3}", atomicSymbols.Count));
			sb.Append (String.Format ("{0, 3}", atoms.Count));
			sb.Append (String.Format ("{0, 3}", bonds.Count));
			sb.AppendLine ("  0  0  0  0  0  0  0  0999 V2000");

			//atom
			foreach (var at in atoms) {
				sb.Append (String.Format ("{0,10:0.0000}", at.X));
				sb.Append (String.Format ("{0,10:0.0000}", at.Y));
				sb.Append (String.Format ("{0,10:0.0000}", at.Z) + " ");
				string atomsymbol = GlycoNAVI.chemical.checkAtomicSymbol(at.Symbol);
				//Console.WriteLine (atomsymbol);
				sb.Append (String.Format ("{0, -2}", atomsymbol));
				sb.AppendLine ("  0  0  0  0  0  0  0  0  0  0  0  0");
			}
			//outsb.AppendLine("wrote atoms in molfile");

			//Bond Line
			for (int j = 0; j < bonds.Count; j++)
			{
				sb.Append(String.Format("{0,3}", bonds[j].fromAtomID));
				sb.Append(String.Format("{0,3}", bonds[j].toAtomID));

				sb.Append(String.Format("{0,3}", (bonds[j].BondType)));
				sb.Append(String.Format("{0,3}", (0)));
				sb.AppendLine("  0  0  0");
				//Console.WriteLine(bonds[j].length);
			}
			//outsb.AppendLine("wrote bonds in molfile");

			sb.AppendLine ("M  END                                                                          ");
			sb.AppendLine ("$$$$");
			// end molfile V2000
			//return sb;
		}
	}
}