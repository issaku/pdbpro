﻿// Xamarin Studio on MAcOSX 10.13.1
// Issaku YAMADA, The Noguchi Institute
using System;
using System.IO;
using System.Text; 
using System.Collections.Generic;
using System.Collections;
using System.IO.Compression;


// PDBpro

namespace GlycoNAVI
{
	class MainClass
	{
		public static void Main (string [] args)
		{

			Boolean checkstructure = false;
			Boolean determineBondOrder = true;

			Console.WriteLine ("start cif to sdf");
			StringBuilder sb = new StringBuilder ();
			StringBuilder sblog = new StringBuilder ();
			string file = String.Empty;
			string [] delimiter = { " " };

			string softname = "PDB chemical component: cif to sdf";
			string softVersion = "Version Sep 29 2017. rev. B";

			sblog.AppendLine ("#\t" + softname);
			sblog.AppendLine ("#\t" + softVersion);

			for (int N = 0; N < Environment.GetCommandLineArgs ().Length; N++) {

				file = Environment.GetCommandLineArgs () [N];

				if (file.Equals ("-h") || file.Equals ("-h")) {
					Console.WriteLine (softname);
					Console.WriteLine (softVersion);
					return;
				}

				//file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/20160608_PDB.components.cif";
				//file = "/Users/yamada/git/BH/BH2016/PDB/CC/wwPDB/20160608_PDB.components.cif";
				//file = "/Users/yamada/git/BH/BH2016/PDB/CC/wwPDB/20160812_PDB.components.cif";
				// PDB_CC_data_010.cif
				//file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/PDB_CC_data_010.cif";
				// 0NM
				//file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/PDB_CC_data_0NM.cif";
				// 0BE
				//file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/PDB_CC_data_0BE.cif";
				// data_6FK.cif
				//file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/PDB_CC_data_6FK.cif";
				// PDB_CC_data_0A.cif
				//file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/PDB_CC_data_0A.cif";
				// PDB_CC_data_A.cif
				//file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/PDB_CC_data_A.cif";
				// PDB_CC_data_UNL-UNM-UNN.cif
				//file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/PDB_CC_data_UNL-UNM-UNN.cif";
				// PDB_CC_data_03S.cif
				//file = "/Users/yamada/Desktop/BH2016/PDB/CC/wwPDB/PDB_CC_data_03S.cif";
				//file = "/Users/yamada/Desktop/wwpdb-cc/test/data_004.cif";

				if (File.Exists (file)) {


					if (file.ToLower ().EndsWith (".cif", StringComparison.OrdinalIgnoreCase)) {

						using (StreamReader reader = new StreamReader (file)) {


							List<string> componentlines = new List<string> ();
							string componentline = String.Empty;


							int inputCount = 1;
							int lineCount = 1;

							Boolean bl_newdata = false;
							//Console.WriteLine ("bl_newdata\t" + bl_newdata);

							string chem_comp_name = string.Empty;
							string chem_comp_formula = string.Empty;
							string InChI = string.Empty;
							string InChIKey = string.Empty;
							string comp_id = string.Empty;


							while ((componentline = reader.ReadLine ()) != null) {



								if (reader.Peek () == -1) {
									bl_newdata = true;
								}

								if (lineCount > 1 && componentline.Length > 4 && componentline.Substring (0, 5).Equals ("data_")) {
									bl_newdata = true;
									inputCount++;
								} else {
									componentlines.Add (componentline);
									lineCount++;
								}

								Boolean bl_coordination_data = true;

								if (bl_newdata == true) {

									string moldata = String.Empty;

									for (int c = 0; c < componentlines.Count; c++) {
										moldata += componentlines [c] + System.Environment.NewLine;
									}

									comp_id = string.Empty;
									bool xyzbl = false;
									int atomCount = 1;
									List<Atom> atoms = new List<Atom> ();
									List<Bond> bonds = new List<Bond> ();

									string [] lines = moldata.Split (new string [] { System.Environment.NewLine }, StringSplitOptions.None);

									for (int j = 0; j < lines.Length; j++) {

										if (lines [j].Length == 8) {
											if (lines [j].Substring (0, 5) == "data_") {

												comp_id = lines [j].Substring (5, 3);
												Console.WriteLine (inputCount + "\tcomp_id: " + comp_id);
											}
										} else if (lines [j].Length == 7) {
											if (lines [j].Substring (0, 5) == "data_") {
												comp_id = lines [j].Substring (5, 2);
												Console.WriteLine (inputCount + "\tcomp_id: " + comp_id);
											}
										} else if (lines [j].Length == 6) {
											if (lines [j].Substring (0, 5) == "data_") {
												comp_id = lines [j].Substring (5, 1);
												Console.WriteLine (inputCount + "\tcomp_id: " + comp_id);
											}
										}


										try {
											if (lines [j].Contains ("_chem_comp.name")) {
												chem_comp_name = lines [j].Substring (50).Replace ("\"", "").Trim ();
											}
										} catch {
											Console.WriteLine ("error chem_comp_name\t" + comp_id);
										}

										try {
											if (lines [j].Contains ("_chem_comp.formula ")) {
												chem_comp_formula = lines [j].Substring (49).Replace ("\"", "").Trim ();
											}
										} catch {
											Console.WriteLine ("error chem_comp_formula\t" + comp_id);
										}

										try {
											if (lines [j].Contains (" InChI            InChI ")) {
												InChI = lines [j].Substring (48).Replace ("\"", "").Trim ();
											}
										} catch {
											Console.WriteLine ("error InChI\t" + comp_id);
										}


										try {
											if (lines [j].Contains (" InChIKey         InChI ")) {
												InChIKey = lines [j].Substring (48).Replace ("\"", "").Trim ();
											}
										} catch {
											Console.WriteLine ("error InChIKey\t" + comp_id);
										}


									}








									if (moldata.Contains ("_chem_comp_atom.pdbx_ordinal")) {

										try {
											List<string> xyzlines = new List<string> ();

											for (int j = 0; j < lines.Length; j++) {

												if (lines [j].Contains ("_chem_comp_atom.pdbx_ordinal")) {
													xyzbl = true;
												}
												if (lines [j].Contains ("#") && xyzbl == true) {
													xyzbl = false;
												}

												if (xyzbl == true) {
													if (lines [j].Length > 64 && (lines [j].Substring (0, 3).Trim () == comp_id
														|| lines [j].Substring (0, 2).Trim () == comp_id
														|| lines [j].Substring (0, 1).Trim () == comp_id
													 )) {
														//_chem_comp_atom.comp_id 
														//_chem_comp_atom.atom_id 
														//_chem_comp_atom.alt_atom_id 
														//_chem_comp_atom.type_symbol 
														//_chem_comp_atom.charge 
														//_chem_comp_atom.pdbx_align 
														//_chem_comp_atom.pdbx_aromatic_flag 
														//_chem_comp_atom.pdbx_leaving_atom_flag 
														//_chem_comp_atom.pdbx_stereo_config 
														//_chem_comp_atom.model_Cartn_x 
														//_chem_comp_atom.model_Cartn_y 
														//_chem_comp_atom.model_Cartn_z 
														//_chem_comp_atom.pdbx_model_Cartn_x_ideal 
														//_chem_comp_atom.pdbx_model_Cartn_y_ideal 
														//_chem_comp_atom.pdbx_model_Cartn_z_ideal 
														//_chem_comp_atom.pdbx_component_atom_id 
														//_chem_comp_atom.pdbx_component_comp_id 
														//_chem_comp_atom.pdbx_ordinal 
														//                          X       Y    Z       X     Y       Z    
														// 0  1   2   3 4 5 6 7 8   9      10    11      12    13      14   15  16  17
														//000 C   C   C 0 1 N N N 32.880 -0.090 51.314 -0.456 0.028  -0.001 C   000 1 
														//000 OA  OA  O 0 1 N N N 34.147 -0.940 51.249 0.662  -0.720 0.001  OA  000 3 

														// 0   1    2    3 4 5 6 7 8   9      10    11       12     13    14   15   16  17
														// 03R C1   C1   C 0 1 Y N N 31.103 7.354  7.198  -1.366 -0.825 -0.321 C1   03R 1  


														string dataline = lines [j];
														dataline = dataline.Replace ("    ", " ");
														dataline = dataline.Replace ("   ", " ");
														dataline = dataline.Replace ("  ", " ");
														//string[] delimiter = { " " };

														xyzlines.Add (dataline);

														//string xyzline = Format.checkQuote (lines [j]);

														//xyzlines.Add (xyzline);
													}
												}
											}

											//foreach (var line in xyzlines) {
											for (int j = 0; j < xyzlines.Count; j++) {

												string [] elements = xyzlines [j].Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

												if (elements.Length > 14) {

													int xp = 12;
													int yp = 13;
													int zp = 14;

													try {
														Atom at = new Atom ();
														at.id = atomCount;

														at.X = double.Parse (elements [xp].Trim ());
														at.Y = double.Parse (elements [yp].Trim ());
														at.Z = double.Parse (elements [zp].Trim ());
														at.atom_site_type_symbol = elements [3].Trim ();
														at.atom_site_label_atom_id_Position = elements [15].Trim ();
														at.atom_site_label_comp_id_molCode = elements [0].Trim ();
														atoms.Add (at);
														atomCount++;
													} catch {

														xp = 9;
														yp = 10;
														zp = 11;

														try {
															Atom at = new Atom ();
															at.id = atomCount;


															at.X = double.Parse (elements [xp].Trim ());
															at.Y = double.Parse (elements [yp].Trim ());
															at.Z = double.Parse (elements [zp].Trim ());
															at.atom_site_type_symbol = elements [3].Trim ();
															at.atom_site_label_atom_id_Position = elements [15].Trim ();
															at.atom_site_label_comp_id_molCode = elements [0].Trim ();
															atoms.Add (at);
															atomCount++;
														} catch {

															if (elements [3].Trim () != "H") {
																sblog.Append ("error\t");
																sblog.AppendLine ("pdb_components:" + comp_id
																+ "\tSymbol: " + elements [3].Trim ()
																+ "\tPosition: " + elements [15].Trim ()
																+ "\t(X,Y,Z: " + elements [xp].Trim ()
																+ "," + elements [yp].Trim ()
																+ "," + elements [zp].Trim () + ")"
																);

																bl_coordination_data = false;
															}
														}
													}
												} else {
													Console.WriteLine ("! if (elements.Length > 14) {");
												}
											}

											//double vdw = GlycoNAVI.chemical.VDW ("H");
											//List<Bond> bonds = new List<Bond> ();
											//bond-order


											if (determineBondOrder == true) {

												int bondid = 1;
												//foreach (var a1 in atoms) {
												for (int j = 0; j < atoms.Count; j++) {

													//foreach (var a2 in atoms) {
													for (int k = 0; k < atoms.Count; k++) {

														if (atoms [j].id > atoms [k].id) {
															double length = 0.0;
															int order = Chemical.bondtype4length (atoms [j], atoms [k], ref length);
															if (order > 0) {
																Bond bd = new Bond ();
																bd.id = bondid;
																bondid++;
																bd.BondOrder = order;
																bd.fromAtomID = atoms [j].id;
																bd.fromAtom = atoms [j].atom_site_type_symbol;
																bd.toAtomID = atoms [k].id;
																bd.toAtom = atoms [k].atom_site_type_symbol;
																bd.length = length;

																if (atoms [k].id == 0 || atoms [j].id == 0) {
																	Console.WriteLine ("bond atom == 0");
																}



																bonds.Add (bd);
															}
														}
													}
												}
											} else {

												//												List<string> bondlines = new List<string> ();

												Boolean bL_bondline = false;
												int bondid = 1;
												//foreach (var line in lines) {
												for (int j = 0; j < lines.Length; j++) {

													if (lines [j].Contains ("_chem_comp_bond.pdbx_ordinal")) {
														bL_bondline = true;
													}
													if (lines [j].Contains ("#")) {
														bL_bondline = false;
													}

													if (bL_bondline == true) {
														if (lines [j].Length > 4 &&
															(lines [j].Substring (0, 3).Trim () == comp_id
															|| lines [j].Substring (0, 2).Trim () == comp_id
															|| lines [j].Substring (0, 1).Trim () == comp_id
															)
														) {
															//if (lines [j].Length > 21 && lines [j].Length < 30 && lines [j].Substring (0, 3) == comp_id) {
															//                          X       Y    Z       X     Y       Z    
															//_chem_comp_bond.comp_id
															//_chem_comp_bond.atom_id_1
															//_chem_comp_bond.atom_id_2
															//_chem_comp_bond.value_order
															//_chem_comp_bond.pdbx_aromatic_flag
															//_chem_comp_bond.pdbx_stereo_config
															//_chem_comp_bond.pdbx_ordinal

															//010 O C  SING N N 1  
															//010 C C6 SING N N 2
															//010 C H  SING N N 3

															string dataline = lines [j];
															dataline = dataline.Replace ("    ", " ");
															dataline = dataline.Replace ("   ", " ");
															dataline = dataline.Replace ("  ", " ");
															string [] elements = dataline.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);


															try {
																Bond bd = new Bond ();
																bd.id = bondid;
																bondid++;

																// bond order
																int t_bOrder = 0;
																//if (lines [j].Substring (9, 4).Equals ("SING")) {
																if (elements [3].Equals ("SING")) {
																	t_bOrder = 1;
																}
																//else if (lines [j].Substring (9, 4).Equals ("DOUB")) {
																else if (elements [3].Equals ("DOUB")) {
																	t_bOrder = 2;
																}
																//else if (lines [j].Substring (9, 4).Equals ("TRIP")) {
																else if (elements [3].Equals ("TRIP")) {
																	t_bOrder = 3;
																}

																// Aromatic
																Boolean b_aromatic = false;
																if (elements [4].Equals ("Y")) {
																	//if (lines [j].Substring (15, 1).Equals ("Y")) {
																	b_aromatic = true;
																	bd.BondOrder = 4;
																} else {
																	bd.BondOrder = t_bOrder;
																}


																Atom fromAtom = new Atom ();
																Atom toAtom = new Atom ();

																for (int k = 0; k < atoms.Count; k++) {
																	if (elements [1].Equals (atoms [k].atom_site_label_atom_id_Position)) {
																		fromAtom = atoms [k];
																	} else if (elements [2].Equals (atoms [k].atom_site_label_atom_id_Position)) {
																		toAtom = atoms [k];
																	} else {

																	}

																}

																try {
																	if (fromAtom.id != 0 && toAtom.id != 0) {
																		//if (fromAtom.id > -1) {
																		if (fromAtom.id > 0) {
																			bd.fromAtomID = fromAtom.id;
																		} else {
																			Console.WriteLine ("atom id: " + fromAtom.id);
																		}


																		bd.fromAtom = fromAtom.atom_site_type_symbol;
																		bd.Aromoatic = b_aromatic;

																		//if (toAtom.id > -1) {
																		if (toAtom.id > 0) {
																			bd.toAtomID = toAtom.id;
																		} else {
																			Console.WriteLine ("atom id: " + toAtom.id);
																		}





																		bd.toAtom = toAtom.atom_site_type_symbol;

																		bonds.Add (bd);
																	}
																}
																catch (Exception ex) {
																	Console.WriteLine (ex.ToString ());
																	
																}



															} catch {
																sblog.AppendLine ("error in bond import");
															}



														}
													}
												}





											}


											if (bl_coordination_data != false) {

												if (checkstructure == true) {
													Chemical.checkBonds (ref atoms, ref bonds);
													Chemical.checkCOCNbond (ref sblog, ref sblog, ref atoms, ref bonds, comp_id);
													bool bl_Valencies = true;
													//Chemical.checkAcetamideStructure (ref sblog, ref sblog, ref atoms, ref bonds, comp_id);

													Chemical.checkAcetamideStructure (ref sblog, ref sblog, ref atoms, ref bonds, "pdb_component:" + comp_id);


													bl_Valencies = Chemical.checkValencies (ref sblog, ref sblog, atoms, bonds);

													if (bl_Valencies == false) {
														Console.WriteLine ("check Valencies:\t" + comp_id);
													}


												} else {
													sblog.AppendLine ("skip amide strcuture check:\t" + comp_id);
												}






												//  (ref StringBuilder log_sb, ref StringBuilder local_error_sb, ref List<Atom> atomdata, ref List<Bond> bonds, string idString)
												//Chemical.checkAcetamideStructure (ref sblog, ref sblog, ref atoms, ref bonds, "pdb_component:" + comp_id);






												if (atoms.Count > 0) {

													GlycoNAVI.Format.Molfile (ref sb, atoms, bonds, "pdb_component:" + comp_id);




													//} // exist moldata




													//}

													sb.AppendLine ("> <PDB_Chemical_Component_ID>");
													sb.AppendLine (comp_id);
													sb.AppendLine ();


													//string chem_comp_name = "";
													//string chem_comp_formula = "";
													//string InChI = "";
													//string InChIKey = "";

													if (chem_comp_name.Length > 0) {
														sb.AppendLine ("> <chem_comp_name>");
														sb.AppendLine (chem_comp_name);
														sb.AppendLine ();
													}

													if (chem_comp_formula.Length > 0) {
														sb.AppendLine ("> <chem_comp_formula>");
														sb.AppendLine (chem_comp_formula);
														sb.AppendLine ();
													}

													if (InChI.Length > 0) {
														sb.AppendLine ("> <InChI>");
														sb.AppendLine (InChI);
														sb.AppendLine ();
													}


													if (InChIKey.Length > 0) {
														sb.AppendLine ("> <InChIKey>");
														sb.AppendLine (InChIKey);
														sb.AppendLine ();
													}



													sb.AppendLine ("$$$$");
												}
											}
											bl_coordination_data = true;

										} catch (Exception ex) {
											Console.WriteLine (ex.ToString ());
										} finally {
											bl_newdata = false;
											componentlines = new List<string> ();
											componentlines.Add (componentline);

											chem_comp_name = "";
											chem_comp_formula = "";
											InChI = "";
											InChIKey = "";
										}



									} // exist moldata
									else {


										// _chem_comp_atom.type_symbol

										for (int c = 0; c < componentlines.Count; c++) {
											moldata += componentlines [c] + System.Environment.NewLine;
										}

										//List<Atom> atoms = new List<Atom> ();
										string chargeString = string.Empty;

										string [] atomlines = moldata.Split (new string [] { System.Environment.NewLine }, StringSplitOptions.None);

										for (int j = 0; j < atomlines.Length; j++) {
											Atom at = new Atom ();
											string elementSymbol = string.Empty;

											if (atomlines [j].Contains ("_chem_comp_atom.type_symbol")) {
												if (atomlines [j].Length > 42) {
													elementSymbol = atomlines [j].Substring (43).Trim ();
													at.atom_site_type_symbol = elementSymbol;
												}
											}
											// _chem_comp_atom.charge
											if (atomlines [j].Contains ("_chem_comp_atom.charge")) {
												if (atomlines [j].Length > 42) {
													chargeString = atomlines [j].Substring (43).Trim ();
													int t_charge = 0;

													Int32.TryParse (chargeString, out t_charge);
													if (t_charge != 0) {
														at.chem_comp_atom_charge = t_charge;
													}
												}
											}

											if (elementSymbol.Length > 0) {
												atoms.Add (at);
												Console.WriteLine (inputCount + "\tcomp_id: " + comp_id);
											}
										}

										if (atoms.Count > 0) {
											GlycoNAVI.Format.Molfile (ref sb, atoms, bonds, "pdb_component:" + comp_id);

											sb.AppendLine ("> <PDB_Chemical_Component_ID>");
											sb.AppendLine (comp_id);
											sb.AppendLine ();

											if (chem_comp_name.Length > 0) {
												sb.AppendLine ("> <chem_comp_name>");
												sb.AppendLine (chem_comp_name);
												sb.AppendLine ();
											}

											if (chargeString.Length > 0) {
												sb.AppendLine ("> <_chem_comp_atom_charge>");
												sb.AppendLine (chargeString);
												sb.AppendLine ();
											}

											sb.AppendLine ("$$$$");
										}


										bl_newdata = false;
										componentlines = new List<string> ();
										componentlines.Add (componentline);

										chem_comp_name = "";
										chem_comp_formula = "";
										InChI = "";
										InChIKey = "";


									}





								} else {
									//bl_newdata = false;



									//componentlines = new List<string> ();
									//componentlines.Add (componentline);
								}



							}
							reader.Close ();
						}



					} else {
						Console.WriteLine ("Please check file type.");
					}
				} else {
					Console.WriteLine ("#\tfile not exist");
				}
			}

			//*
			DateTime dt = DateTime.Now;
			string datetimesting = dt.ToString ("MM-dd-yyyy-HH-mm");

			string check = ".str_check.";
			if (checkstructure == false) { 
				check = ".str-non-check.";
			}


			StreamWriter writer = new StreamWriter (file + "_PDB_cif2sdf."+ check + datetimesting + ".sdf");

			writer.Write (sb.ToString ());
			writer.Close ();

			StreamWriter writerlog = new StreamWriter (file + "_PDB_cif2sdf." + check + datetimesting + ".sdf.log.txt");

			writerlog.Write (sblog.ToString ());
			writerlog.Close ();
			//*/

			Console.WriteLine ("finished");
		}
	}
}
