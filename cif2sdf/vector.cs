﻿using System;
using System.ComponentModel;

namespace GlycoNAVI
{

	[Serializable]
	public struct Vector
	{
		public double X;
		public double Y;
		public Vector(double X, double Y)
		{
			this.X = X;
			this.Y = Y;
		}
			
		public static Vector operator -(Vector vector1, Vector vector2)
		{
			return new Vector(vector1.X - vector2.X, vector1.Y - vector2.Y);
		}

		public double Length
		{
			get
			{
				return Math.Sqrt((this.X * this.X) + (this.Y * this.Y));
			}
		}


		public double LengthSquared
		{
			get
			{
				return ((this.X * this.X) + (this.Y * this.Y));
			}
		}

		public static Vector Add(Vector vector1, Vector vector2)
		{
			return new Vector(vector1.X + vector2.X, vector1.Y + vector2.Y);
		}


		public static double AngleBetween(Vector vector1, Vector vector2)
		{
			double Y = (vector1.X * vector2.Y) - (vector2.X * vector1.Y);
			double x = (vector1.X * vector2.X) + (vector1.Y * vector2.Y);
			return (Math.Atan2(Y, x) * 180.0 / Math.PI);
		}





		/// <summary>
		/// Vectorの内積の計算
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <returns></returns>
		public static double InnerProduct(double[] v1, double[] v2)
		{
			double ip = 0;

			if (v1.Length != v2.Length)
				throw new ArgumentException("配列の次元は同じにして下さい", "v1, v2");

			for (int i = 0; i < v1.Length; i++)
			{
				ip += v1[i] * v2[i];
			}
			return ip;
		}
		/// <summary>
		/// Vectorの内積の計算
		/// </summary>
		/// <param name="v1"></param>
		/// <param name="v2"></param>
		/// <returns></returns>
		public static double InnerProduct(Vector v1, Vector v2)
		{
			double ip = 0;

			if (v1.Length != v2.Length)
				throw new ArgumentException("配列の次元は同じにして下さい", "v1, v2");

			ip = (v1.X * v2.X) + (v1.Y = v2.Y);

			return ip;
		}


		public static double CrossProduct(Vector vector1, Vector vector2)
		{
			return (vector1.X * vector2.Y) - (vector1.Y * vector2.X);
		}


		public static double Determinant(Vector vector1, Vector vector2)
		{
			return ((vector1.X * vector2.Y) - (vector1.Y * vector2.X));
		}
	





		//ベクトルの定義
		public  struct Vector3D{
			public double x;
			public double y;
			public double z;
		};

		//ベクトルの長さを計算する
		public static  double get_vector_length( Vector3D v ) {
			return Math.Pow( ( v.x * v.x ) + ( v.y * v.y ) + ( v.z * v.z ), 0.5 );
		}

		//ベクトル内積
		public static double dot_product(Vector3D vl, Vector3D vr) {
			return vl.x * vr.x + vl.y * vr.y + vl.z * vr.z;
		}

		//２つのベクトルABのなす角度θを求める;0～180の角度
		public static double AngleOf2Vector(Vector3D A, Vector3D B )
		{
			//※ベクトルの長さが0だと答えが出ませんので注意してください。

			//ベクトルAとBの長さを計算する
			double length_A = get_vector_length(A);
			double length_B = get_vector_length(B);

			//内積とベクトル長さを使ってcosθを求める
			double cos_sita = dot_product(A,B) / ( length_A * length_B );

			//cosθからθを求める
			double sita = Math.Acos( cos_sita );	

			//ラジアンでなく0～180の角度でほしい場合はコメント外す
			sita = sita * 180.0 / Math.PI;

			return sita;
		}
	}

}
