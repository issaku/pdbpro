﻿using System;

namespace GlycoNAVI
{
	public class AminoAcidResidue
	{
		public static string[] Residue = 
		{
			"ASN"
			, "ARG"
			, "THR"
			, "SER"
			, "HYP"
			, "LYZ"
			, "TRP"
		};
	}
}

