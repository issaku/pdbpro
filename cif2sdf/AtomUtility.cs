﻿// Xamarin Studio on MAcOSX 10.9
// Issaku YAMADA, Noguchi Institute
using System;
using GlycoNAVI;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Text;

namespace GlycoNAVI
{
	public class AtomUtility
	{

		public static List<Atom> SetAtomIds(List<Atom> a_atoms){
			int count = 1;
			for (int i = 0; i < a_atoms.Count; i++) {
				a_atoms [i].id = count;
				count++;
			}
			return a_atoms;
		}


		public static void SeparateModel(ref StringBuilder log_sb, List<string> pdbx_PDB_model_num_list, List<Atom> mmcifAtoms, ref List<Model> pdb_model){

			try {
				for (int i = 0; i < pdbx_PDB_model_num_list.Count; i++) {

					Model mod = new Model ();
					List<Atom> atoms = new List<Atom> ();


					for (int j = 0; j < mmcifAtoms.Count; j++) {

						if (pdbx_PDB_model_num_list [i] == mmcifAtoms [j].pdbx_PDB_model_num) {
							atoms.Add (mmcifAtoms [j]);					
						}
					}

					mod.Addatom (atoms);
					mod.model_index = pdbx_PDB_model_num_list [i];

					pdb_model.Add (mod);
				}
			} catch (Exception ex) {
				System.Diagnostics.Debug.WriteLine (ex.ToString ());
				log_sb.AppendLine (ex.ToString ());
			}

		}

		// AtomUtility.GetModelEntryAtoms (ref mmcif_pdb_model_sugar_entry, pdb_model [modelInt].atom_list, SUGAR_Enrty_ids);
		public static void GetModelEntityAtoms (ref StringBuilder log_sb, ref List<Model_Entity> a_mmcif_pdb_model_sugar_entry, Model modelAtoms, List<string> a_SUGAR_Enrty_ids) {
		

			for (int j = 0; j < a_SUGAR_Enrty_ids.Count; j++) {
				Model_Entity model_entry = new Model_Entity ();
				List<Atom> a_atoms = new List<Atom> ();
//				string str_model_index = string.Empty;

				for (int i = 0; i < modelAtoms.atom_list.Count; i++) {
					try {
						if (modelAtoms.atom_list [i].hetAtom == true) {

							if (a_SUGAR_Enrty_ids[j] == modelAtoms.atom_list [i].atom_site_label_entity_id ) {

								a_atoms.Add (modelAtoms.atom_list [i]);
							}
						}
					} catch (Exception ex) {
						System.Diagnostics.Debug.WriteLine (ex.ToString ());
						log_sb.AppendLine (ex.ToString ());
					}
				}
				model_entry.Addatom (a_atoms);
				model_entry.entity_index = a_SUGAR_Enrty_ids [j];
				model_entry.model_index = modelAtoms.model_index;
				a_mmcif_pdb_model_sugar_entry.Add (model_entry);
			}
		}



		public static void GetAtoms (string hatom, ref int atomCount, ref List<Atom> atoms, List<string> a_SUGAR_Enrty_ids, List<string> GLYCOSYLATION_SITES_seq_id)
		{
			// アミノ酸種類ごとに処理を行うのではなく、アミノ酸側鎖のヘテロアトムを抽出する。＆
			// C-Glycan (C-マンノシル化)への対応を行うほうが現実的かも？
			try {

				// GLYCOSYLATION_SITES


				/*
								if (hatom.Contains ("ATOM") ) {
									//GLYCOSYLATION_SITES_seq_id
									Atom at = new Atom ();
									at.id = atomCount;
									atomCount++;

									hatom = hatom.Replace ("    ", " ");
									hatom = hatom.Replace ("   ", " ");
									hatom = hatom.Replace ("  ", " ");
									string[] delimiter = { " " };
									string[] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

									// ATOM   4713 N  N   . ASN E  1  80  ? 85.744  16.457  46.245  1.00 73.47  ? ? ? ? ? ? 80   ASN E N   1 
									// ATOM   4714 C  CA  . ASN E  1  80  ? 85.055  17.050  45.073  1.00 72.55  ? ? ? ? ? ? 80   ASN E CA  1 
									// ATOM   4715 C  C   . ASN E  1  80  ? 84.668  15.928  44.083  1.00 70.19  ? ? ? ? ? ? 80   ASN E C   1 
									// ATOM   4716 O  O   . ASN E  1  80  ? 84.339  16.221  42.936  1.00 71.68  ? ? ? ? ? ? 80   ASN E O   1 
									// ATOM   4717 C  CB  . ASN E  1  80  ? 85.904  18.035  44.234  1.00 76.09  ? ? ? ? ? ? 80   ASN E CB  1 
									// ATOM   4718 C  CG  . ASN E  1  80  ? 86.808  18.923  45.041  1.00 74.92  ? ? ? ? ? ? 80   ASN E CG  1 
									// ATOM   4719 O  OD1 . ASN E  1  80  ? 88.031  18.873  44.860  1.00 69.68  ? ? ? ? ? ? 80   ASN E OD1 1 
									// ATOM   4720 N  ND2 . ASN E  1  80  ? 86.230  19.832  45.810  1.00 75.88  ? ? ? ? ? ? 80   ASN E ND2 1 


									if (GLYCOSYLATION_SITES_seq_id.Contains(elements [8])
									&&
										( (hatom.Contains(". ASN") && elements[3] == "ND2")
											|| (( hatom.Contains(". ARG")) &&  ( elements[3] == "NH1" || elements[3] == "NH2")) 
											|| ( hatom.Contains(". THR") && elements[3] == "OG1" )
											|| ( hatom.Contains(". SER") && elements[2] == "O" )
											|| ( hatom.Contains(". HYP") && elements[2] == "O" )
											|| ( hatom.Contains(". LYZ") && elements[2] == "O" )
											|| ( hatom.Contains(". TRP") && elements[3] != "CA" && elements[2] == "C" )
										)

									){
										at.X = double.Parse(elements[10]);
										at.Y = double.Parse(elements[11]);
										at.Z = double.Parse(elements[12]);								
										at.Symbol   = elements [2];
										at.Position = elements [3];
										at.molCode  = elements [5];
										at.asym_id = elements [6];
										at.entity_id = elements [7];
										at.seq_id = elements [8];
										at.atom_index = elements [1];
										at.atom_site_occupancy = elements [13];
										atoms.Add (at);
									}




								}
				*/



				//N-Glycan
				if (hatom.Length > 3) {
					if (hatom.Substring (0, 4) == "ATOM"
						&& (hatom.Contains (". ASN") || hatom.Contains (". ARG")) //http://onlinelibrary.wiley.com/doi/10.1002/anie.201407824/abstract
						&& hatom.Contains (" N ")                                //アルギニン結合Ｎ-グリカン
																				 //	&& hatom.Length > 94
					) {

						Atom at = new Atom ();
						at.id = atomCount;
						atomCount++;

						hatom = hatom.Replace ("    ", " ");
						hatom = hatom.Replace ("   ", " ");
						hatom = hatom.Replace ("  ", " ");
						string [] delimiter = { " " };
						string [] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

						at.X = double.Parse (elements [10]);
						at.Y = double.Parse (elements [11]);
						at.Z = double.Parse (elements [12]);
						at.atom_site_occupancy = double.Parse (elements [13]);
						at.atom_site_type_symbol = elements [2];
						at.atom_site_label_atom_id_Position = elements [3];
						at.atom_site_label_comp_id_molCode = elements [5];
						at.atom_site_label_asym_id = elements [6];
						at.atom_site_label_entity_id = elements [7];
						at.atom_site_label_seq_id = elements [8];
						at.atom_site_id = elements [1];
						at.pdbx_PDB_model_num = elements [25].Trim ();
						at.hetAtom = false;
						//atoms.Add (at);

					}
					//O-Glycan HYP:(4R)-4-hydroxy-L-proline, 5-HYDROXYLYSINE (LYZ)
					// http://www.chem.qmul.ac.uk/iupac/misc/glycp.html
					else if (hatom.Substring (0, 4) == "ATOM"
						&& (hatom.Contains (". THR") || hatom.Contains (". SER") || hatom.Contains (". HYP") || hatom.Contains (". LYZ"))
						&& hatom.Contains (" O ")
					//	&& hatom.Length > 94
					) {

						Atom at = new Atom ();
						at.id = atomCount;
						atomCount++;

						hatom = hatom.Replace ("    ", " ");
						hatom = hatom.Replace ("   ", " ");
						hatom = hatom.Replace ("  ", " ");
						string [] delimiter = { " " };
						string [] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

						at.X = double.Parse (elements [10]);
						at.Y = double.Parse (elements [11]);
						at.Z = double.Parse (elements [12]);
						at.atom_site_occupancy = double.Parse (elements [13]);
						at.atom_site_type_symbol = elements [2];
						at.atom_site_label_atom_id_Position = elements [3];
						at.atom_site_label_comp_id_molCode = elements [5];
						at.atom_site_label_asym_id = elements [6];
						at.atom_site_label_entity_id = elements [7];
						at.atom_site_label_seq_id = elements [8];
						at.atom_site_id = elements [1];
						at.pdbx_PDB_model_num = elements [25].Trim ();
						at.hetAtom = false;
						//atoms.Add (at);

					}
					// C-Glycan
					else if (hatom.Substring (0, 4) == "ATOM"
						&& hatom.Contains (". TRP") // C-マンノシル化
						&& hatom.Contains (" C ")
					//	&& hatom.Length > 94
					) {

						Atom at = new Atom ();
						at.id = atomCount;
						atomCount++;

						hatom = hatom.Replace ("    ", " ");
						hatom = hatom.Replace ("   ", " ");
						hatom = hatom.Replace ("  ", " ");
						string [] delimiter = { " " };
						string [] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

						at.X = double.Parse (elements [10]);
						at.Y = double.Parse (elements [11]);
						at.Z = double.Parse (elements [12]);
						at.atom_site_occupancy = double.Parse (elements [13]);
						at.atom_site_type_symbol = elements [2];
						at.atom_site_label_atom_id_Position = elements [3];
						at.atom_site_label_comp_id_molCode = elements [5];
						at.atom_site_label_asym_id = elements [6];
						at.atom_site_label_entity_id = elements [7];
						at.atom_site_label_seq_id = elements [8];
						at.atom_site_id = elements [1];
						at.pdbx_PDB_model_num = elements [25].Trim ();
						at.hetAtom = false;
						//atoms.Add (at);

					}
				}
				if (hatom.Length > 5) {
					if (hatom.Substring (0, 6) == "HETATM" //hatom.Contains ("HETATM") 
					&& !hatom.Contains (" HOH ")
					//	&& hatom.Length > 94
					) {
						//0         1         2         3         4         5         6         7         8         9  
						//0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
						//mmcif
						//HETATM 3685 C  C1  . NAG C 2 .   ? 61.544 4.886  39.807  1.00 59.73 ? ? ? ? ? ? 801 NAG A C1  1 
						//  [0] _atom_site.group_PDB 
						//  [1] _atom_site.id 
						//  [2] _atom_site.type_symbol 
						//  [3] _atom_site.label_atom_id 
						//  [4] _atom_site.label_alt_id 
						//  [5] _atom_site.label_comp_id 
						//  [6] _atom_site.label_asym_id 
						//  [7] _atom_site.label_entity_id 
						//  [8] _atom_site.label_seq_id 
						//  [9] _atom_site.pdbx_PDB_ins_code 
						// [10] _atom_site.Cartn_x 
						// [11] _atom_site.Cartn_y 
						// [12] _atom_site.Cartn_z 
						// [13] _atom_site.occupancy 
						// [14] _atom_site.B_iso_or_equiv 
						// [15] _atom_site.Cartn_x_esd 
						// [16] _atom_site.Cartn_y_esd 
						// [17] _atom_site.Cartn_z_esd 
						// [18] _atom_site.occupancy_esd 
						// [19] _atom_site.B_iso_or_equiv_esd 
						// [20] _atom_site.pdbx_formal_charge 
						// [21] _atom_site.auth_seq_id 
						// [22] _atom_site.auth_comp_id 
						// [23] _atom_site.auth_asym_id 
						// [24] _atom_site.auth_atom_id 
						// [25] _atom_site.pdbx_PDB_model_num

						//61.544     4.886  .544 4.8  C  0  0  0  0  0  0  0  0  0  0  0  0
						if (hatom.Length > 12) {
							if (hatom.Substring (12, 2).Trim ().ToUpper () != "H") {

								try {
									Atom at = new Atom ();
									at.id = atomCount;
									atomCount++;

									hatom = hatom.Replace ("    ", " ");
									hatom = hatom.Replace ("   ", " ");
									hatom = hatom.Replace ("  ", " ");
									string [] delimiter = { " " };
									string [] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

									at.X = double.Parse (elements [10]);
									at.Y = double.Parse (elements [11]);
									at.Z = double.Parse (elements [12]);
									at.atom_site_occupancy = double.Parse (elements [13]);

									string atomsymbol = "*";

									try {
										atomsymbol = GlycoNAVI.Chemical.checkAtomicSymbol (elements [2]);
										if (atomsymbol != "*") {
											at.atom_site_type_symbol = elements [2];
											at.atom_site_label_atom_id_Position = elements [3];
											at.atom_site_label_comp_id_molCode = elements [5];
											at.atom_site_label_asym_id = elements [6];
											at.atom_site_label_entity_id = elements [7];
											at.atom_site_label_seq_id = elements [8];
											at.atom_site_id = elements [1];
											at.pdbx_PDB_model_num = elements [25].Trim ();
											at.hetAtom = true;

											if (a_SUGAR_Enrty_ids.Contains (at.atom_site_label_entity_id)) {
												atoms.Add (at);
											}
										}
									} catch (Exception ex) {
										System.Diagnostics.Debug.WriteLine (ex.ToString ());
									}
								} catch (Exception ex) {

									System.Diagnostics.Debug.WriteLine (ex.ToString ());

								}
							}
						}
					}
				}

			} catch (Exception ex) {

				System.Diagnostics.Debug.WriteLine (ex.ToString ());

			}
		}


		public static void Get_mmcif_atoms (string hatom, ref int atomCount, ref List<Atom> mmcifAtoms) //List<Model> allmodels, List<string> pdbx_PDB_model_num_list)
		{


			try {

				//				Model[] moldels = new Model[pdbx_PDB_model_num_list.Count];

				//				for (int i = 0; i < pdbx_PDB_model_num_list.Count; i++) {

				//					Model mod = new Model();
				//				List<Atom> mmcifAtoms = new List<Atom> ();
				if (hatom.Length > 3) {
					if (hatom.Substring (0, 4) == "ATOM"// hatom.Contains ("ATOM")
														//     && hatom.Length > 94
					) {

						Atom at = new Atom ();
						at.id = atomCount;
						atomCount++;

						hatom = hatom.Replace ("    ", " ");
						hatom = hatom.Replace ("   ", " ");
						hatom = hatom.Replace ("  ", " ");
						string [] delimiter = { " " };
						string [] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

						// ATOM   396  N  N   . ASN A  1  49  ? 46.791  -1.778  7.416   1.00 83.58  ? ? ? ? ? ? 49   ASN A N   1 

						//						if (elements [25].Trim () == pdbx_PDB_model_num_list [i]) {

						at.X = double.Parse (elements [10]);
						at.Y = double.Parse (elements [11]);
						at.Z = double.Parse (elements [12]);
						at.atom_site_occupancy = double.Parse (elements [13]);
						at.atom_site_type_symbol = elements [2].Trim ();
						at.atom_site_label_atom_id_Position = elements [3].Trim ();
						at.atom_site_label_comp_id_molCode = elements [5].Trim ();
						at.atom_site_label_asym_id = elements [6].Trim ();
						at.atom_site_label_entity_id = elements [7].Trim ();
						at.atom_site_label_seq_id = elements [8].Trim ();
						at.atom_site_id = elements [1].Trim ();
						at.pdbx_PDB_model_num = elements [25].Trim ();
						at.hetAtom = false;


						//							if (moldels[Convert.ToInt32(pdbx_PDB_model_num_list[i]) - 1 ] != null && moldels[Convert.ToInt32(pdbx_PDB_model_num_list[i]) - 1 ].atom_list != null) {
						//
						//								atoms = moldels[Convert.ToInt32(pdbx_PDB_model_num_list[i]) - 1 ].atom_list;
						//							}
						mmcifAtoms.Add (at);


						//							moldels[Convert.ToInt32(pdbx_PDB_model_num_list[i]) - 1 ].Addatom(atoms);
						//							mod.atom_list = atoms;
						//							mod.model_index = at.pdbx_PDB_model_num;
						//						}

					}
				}


				//				Console.WriteLine ("mmcifAtoms.Count\t" + mmcifAtoms.Count);
				//allmodels.Add (mod)// 

				//				}

				//				for (int i = 0; i < moldels.Length; i++) {
				//					allmodels.Add(moldels[i]);
				//				}


				// set Oxygen atoms  HOH
				if (hatom.Length > 5) {
					if (hatom.Substring (0, 6) == "HETATM" //hatom.Contains ("HETATM") 
						&& !hatom.Contains (" HOH ")
					//	&& hatom.Length > 94
					) {
						//0         1         2         3         4         5         6         7         8         9  
						//0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
						//mmcif
						//HETATM 3685 C  C1  . NAG C 2 .   ? 61.544 4.886  39.807  1.00 59.73 ? ? ? ? ? ? 801 NAG A C1  1 
						//HETATM 6757 O  O   . HOH EA 10 .   ? 58.963  10.572  18.813  1.00 60.45  ? ? ? ? ? ? 3042 HOH C O   1 
						//  [0] _atom_site.group_PDB 
						//  [1] _atom_site.id 
						//  [2] _atom_site.type_symbol 
						//  [3] _atom_site.label_atom_id 
						//  [4] _atom_site.label_alt_id 
						//  [5] _atom_site.label_comp_id 
						//  [6] _atom_site.label_asym_id 
						//  [7] _atom_site.label_entity_id 
						//  [8] _atom_site.label_seq_id 
						//  [9] _atom_site.pdbx_PDB_ins_code 
						// [10] _atom_site.Cartn_x 
						// [11] _atom_site.Cartn_y 
						// [12] _atom_site.Cartn_z 
						// [13] _atom_site.occupancy 
						// [14] _atom_site.B_iso_or_equiv 
						// [15] _atom_site.Cartn_x_esd 
						// [16] _atom_site.Cartn_y_esd 
						// [17] _atom_site.Cartn_z_esd 
						// [18] _atom_site.occupancy_esd 
						// [19] _atom_site.B_iso_or_equiv_esd 
						// [20] _atom_site.pdbx_formal_charge 
						// [21] _atom_site.auth_seq_id 
						// [22] _atom_site.auth_comp_id 
						// [23] _atom_site.auth_asym_id 
						// [24] _atom_site.auth_atom_id 
						// [25] _atom_site.pdbx_PDB_model_num

						//61.544     4.886  .544 4.8  C  0  0  0  0  0  0  0  0  0  0  0  0

						//if (hatom.Substring (12, 2).Trim ().ToUpper() != "H") {

						try {
							Atom at = new Atom ();
							at.id = atomCount;
							atomCount++;

							hatom = hatom.Replace ("    ", " ");
							hatom = hatom.Replace ("   ", " ");
							hatom = hatom.Replace ("  ", " ");
							string [] delimiter = { " " };
							string [] elements = hatom.Split (delimiter, StringSplitOptions.RemoveEmptyEntries);

							//if (elements [5] == "HOH") {

							at.X = double.Parse (elements [10]);
							at.Y = double.Parse (elements [11]);
							at.Z = double.Parse (elements [12]);
							at.atom_site_occupancy = double.Parse (elements [13]);

							string atomsymbol = "*";

							try {
								atomsymbol = GlycoNAVI.Chemical.checkAtomicSymbol (elements [2]);
								if (atomsymbol != "*") {
									at.atom_site_type_symbol = elements [2];
									at.atom_site_label_atom_id_Position = elements [3];
									at.atom_site_label_comp_id_molCode = elements [5];
									at.atom_site_label_asym_id = elements [6];
									at.atom_site_label_entity_id = elements [7];
									at.seq_id = elements [8];
									at.atom_site_label_seq_id = elements [8].Trim ();
									at.pdbx_PDB_model_num = elements [25].Trim ();
									at.atom_site_id = elements [1].Trim ();
									at.hetAtom = true;
									mmcifAtoms.Add (at);
								}
							} catch (Exception ex) {
								System.Diagnostics.Debug.WriteLine (ex.ToString ());
							}


							//}



						} catch (Exception ex) {

							System.Diagnostics.Debug.WriteLine (ex.ToString ());

						}
						//}
					}
				}


			} catch (Exception ex) {
				System.Diagnostics.Debug.WriteLine (ex.ToString ());
			}
		}



		public static void GetModelEntityAsymAtoms(ref List<Model_Entity_Asym> a_Model_Entry_Asyms, Model_Entity Model_Entry)
		{
			List<string> a_sym_ids = new List<string> ();

			for (int j = 0; j < Model_Entry.atom_list.Count; j++) {
				if (!a_sym_ids.Contains (Model_Entry.atom_list [j].atom_site_label_asym_id)) {
					a_sym_ids.Add (Model_Entry.atom_list [j].atom_site_label_asym_id);
				}			
			}


			for (int i = 0; i < a_sym_ids.Count; i++) {

				List<Atom> t_atoms = new List<Atom> ();
				string enrty_index = string.Empty;
				string model_index = string.Empty;

				for (int j = 0; j < Model_Entry.atom_list.Count; j++) {
					
					if (Model_Entry.atom_list [j].atom_site_label_asym_id == a_sym_ids [i]) {
						t_atoms.Add (Model_Entry.atom_list [j]);
						enrty_index = Model_Entry.entity_index;
						model_index = Model_Entry.model_index;
					}
				}
				Model_Entity_Asym asm = new Model_Entity_Asym ();
				asm.Addatom (t_atoms);
				asm.asym_id = a_sym_ids [i];
				asm.entity_index = enrty_index;
				asm.model_index = model_index;

				a_Model_Entry_Asyms.Add (asm);
			}
		}


		public static void SetAtomToArray_SeqId (List<Atom> atoms, ref List<List<Atom>> a_array_atoms, List<string> a_SUGAR_Enrty_ids, List<string> GLYCOSYLATION_SITES_seq_id)
		{
			for(int j = 0; j < a_SUGAR_Enrty_ids.Count; j++){
			//foreach (var entryId in a_SUGAR_Enrty_ids) {
				List<Atom> t_atoms = new List<Atom> ();
//				string asym_id = "";

				for(int i = 0; i < atoms.Count; i++){
				//foreach (var at in atoms) {
//					if ( at.Position == "C1" || at.Position == "C2") {
//						asym_id = at.asym_id;
//					}

					if (atoms[i].atom_site_label_entity_id == a_SUGAR_Enrty_ids[j]) {
						t_atoms.Add (atoms[i]);					
					}

					for(int k = 0; k < AminoAcidResidue.Residue.Length; k++){
					//foreach (var code in AminoAcidResidue.Residue) {
						if (AminoAcidResidue.Residue[k] == atoms[i].atom_site_label_comp_id_molCode) {
							if (GLYCOSYLATION_SITES_seq_id.Contains(atoms[i].atom_site_label_seq_id)){
								t_atoms.Add (atoms[i]); // for amino acid residue
							}
						}
					}
				}

				int id = 1;
				List<Atom> new_atoms = new List<Atom> ();

				for(int i = 0; i < t_atoms.Count; i++){
				//foreach (var t_at in t_atoms) {
//					if (asym_id == t_at.asym_id) {
					t_atoms[i].id = id;
						id++;
					new_atoms.Add (t_atoms[i]);
//					}
				}


				a_array_atoms.Add (new_atoms);
			}
		}


		public static List<Atom> AddAglyconAnomericCarbons(List<Atom> a_atoms, List<Atom> a_all_atoms){

			List<Atom> atoms = new List<Atom> ();

			//int check_count = 1;

			for (int i = 0; i < a_atoms.Count; i++) {
				//foreach (var at in a_atoms) {

				try {
					if (a_atoms [i].atom_site_label_atom_id_Position == "C1" || a_atoms [i].atom_site_label_atom_id_Position == "C2") {

//						Console.WriteLine (i + "\t" + a_atoms [i].atom_id_Position + "\t" + a_atoms [i].comp_id_molCode + ":" + a_atoms [i].atom_index);

						for (int j = 0; j < a_all_atoms.Count; j++) {
							//foreach (var alat in a_all_atoms) {
							//Console.WriteLine("\t"+ j + "\t" + a_all_atoms [j].auth_comp_id + ":" + a_all_atoms [j].atom_id);

//							bool db_res = false; 
							foreach (var res in  AminoAcidResidue.Residue) {
								if (res == a_all_atoms [j].atom_site_label_comp_id_molCode) {
									//db_res = true;
									//continue;
									//}
									//}

									//if (db_res == true) {

									//Console.WriteLine("" + check_count);
									//check_count++;

									double length = 0.0;
									int order = Chemical.bondtype4length (a_atoms [i], a_all_atoms [j], ref length);



									if (order > 0) {
										if (a_atoms [i].atom_site_label_atom_id_Position != a_all_atoms [j].atom_site_label_atom_id_Position) {

											if (!CheckResidue (atoms, a_all_atoms [j])){
												atoms.Add (a_all_atoms [j]);
											}

//											Console.WriteLine ("order:\t" + order);

											// if add amino acid residue atoms
											for (int k = 0; k < GetResidue (a_all_atoms, a_all_atoms [j]).Count; k++) {
												//foreach (var amino in GetResidue(a_all_atoms, a_all_atoms[j])) {

												if (CheckResidue (atoms, GetResidue (a_all_atoms, a_all_atoms [j]) [k])) {
													atoms.Add (GetResidue (a_all_atoms, a_all_atoms [j]) [k]);
												}

												//Console.WriteLine("atoms.Count\t"+ atoms.Count);
											}
											// if add amino acid residue atoms

										}
									}
								}
							}
						}
					}
				} catch (Exception ex) {
					Console.WriteLine (ex.ToString ());
				}


			}

			return atoms;
		}


		public static List<Atom> GetResidue(List<Atom> a_all_atoms, Atom atom){
		
			List<Atom> atoms = new List<Atom>();

			for (int j = 0; j < a_all_atoms.Count; j++) {
			//foreach (var at in a_all_atoms) {

				try {
					if (a_all_atoms[j].atom_site_label_asym_id == atom.atom_site_label_asym_id && a_all_atoms[j].atom_site_label_seq_id == atom.atom_site_label_seq_id && a_all_atoms[j].atom_site_id != atom.atom_site_id) {
						atoms.Add(a_all_atoms[j]);
					}
				}
				catch (Exception ex) {
					Console.WriteLine (ex.ToString ());
				}
			}

			return atoms;
		}



		public static bool CheckResidue(List<Atom> a_all_atoms, Atom atom){

			bool bl_check = false;

			for (int j = 0; j < a_all_atoms.Count; j++) {
				//foreach (var at in a_all_atoms) {

				try {
					if (a_all_atoms[j].atom_site_label_asym_id == atom.atom_site_label_asym_id 
						&& a_all_atoms[j].atom_site_label_seq_id == atom.atom_site_label_seq_id 
						&& a_all_atoms[j].atom_site_id != atom.atom_site_id) {

						bl_check = true;
					}
				}
				catch (Exception ex) {
					Console.WriteLine (ex.ToString ());
				}
			}

			return bl_check;
		}


		public static Atom SetAtomData(Atom a_atom){
			Atom at = new Atom ();

			at.X = a_atom.X;
			at.Y = a_atom.Y;
			at.Z = a_atom.Z;
			at.atom_site_occupancy = a_atom.atom_site_occupancy;
			at.atom_site_type_symbol = a_atom.atom_site_type_symbol;
			at.atom_site_label_atom_id_Position = a_atom.atom_site_label_atom_id_Position;
			at.atom_site_label_comp_id_molCode = a_atom.atom_site_label_comp_id_molCode;
			at.atom_site_label_asym_id = a_atom.atom_site_label_asym_id;
			at.atom_site_label_entity_id = a_atom.atom_site_label_entity_id;
			at.atom_site_label_seq_id = a_atom.atom_site_label_seq_id;
			at.atom_site_id = a_atom.atom_site_id;
			at.pdbx_PDB_model_num = a_atom.pdbx_PDB_model_num;
			at.hetAtom = a_atom.hetAtom;
			at.aromatic = a_atom.aromatic;
			at.seq_id = a_atom.seq_id;
			at.is_terminal_heteroatom = a_atom.is_terminal_heteroatom;
			at.id = a_atom.id;
			at.hybrid = a_atom.hybrid;
			at.description = a_atom.description;
			at.comp_id = a_atom.comp_id;
			at.auth_comp_id = a_atom.auth_comp_id;
			at.atom_id = a_atom.atom_id;

		
			return at;
		}


	}
}

