﻿// Xamarin Studio on MAcOSX 10.9
// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text; 
using System.Collections.Generic;
using System.Collections;
using System.IO.Compression;
//using GlycoNAVI;
namespace get_pdb_components_id
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			StringBuilder sb = new StringBuilder ();
			string file = String.Empty;
			//string dirname = String.Empty;
			DateTime dt = DateTime.Now;
			string datetimesting = dt.ToString ("MMddyyHHmm");
			//outsb.AppendLine ("start: " + datetimesting);

			for (int N = 1; N < Environment.GetCommandLineArgs ().Length; N++) {

				file = Environment.GetCommandLineArgs () [N];

				if (File.Exists (file)) {

						using (StreamReader reader = new StreamReader (file)) {

						string line = String.Empty;

						while ((line = reader.ReadLine ()) != null) {
						
							if (line.Contains ("InChIKey")) {
								sb.AppendLine (line);
							}
						}


						}

				}
			}


			//string endtimesting = dt.ToString ("MMddyyHHmm");
			//sb.AppendLine ("finished: " + endtimesting);
			StreamWriter writer = new StreamWriter("PDB_components_InChIKey." + datetimesting + ".txt") ;

			writer.Write(sb.ToString()) ;
			writer.Close() ;

			Console.WriteLine ("finished");






		}
	}
}
