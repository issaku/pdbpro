﻿// Xamarin Studio on MAcOSX 10.9
// Issaku YAMADA, Noguchi Institute
using System;
using System.IO;
using System.Text; 
using System.Collections.Generic;
using System.Collections;
using System.IO.Compression;

namespace getEePDBCode
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			StringBuilder sb = new StringBuilder ();

			string filename = "20160426.log.gz";

			FileStream inStream // 入力ストリーム
			= new FileStream (filename, FileMode.Open, FileAccess.Read);

			System.IO.FileInfo cFileInfo = new System.IO.FileInfo(filename);
			string stExtension = cFileInfo.Extension;

			Stream str = inStream;

			if (stExtension == ".gz") {
				GZipStream decompStream // 解凍ストリーム
				= new GZipStream (
					inStream, // 入力元となるストリームを指定
					CompressionMode.Decompress); // 解凍（圧縮解除）を指定
				str = decompStream;
			}

			string line = String.Empty;

			using (StreamReader reader = new StreamReader (str)) {
				int count = 1;
				while ((line = reader.ReadLine ()) != null) {
					if (line.Contains ("\tER")) {
						try {
							Console.WriteLine(count + ":\t" + line.Substring(0,4));
							count++;

							sb.AppendLine (line.Substring(0,4));
						}
						catch (Exception ex) {
							Console.WriteLine (ex.ToString ());
						}
					}		
				}
			}

			using (StreamWriter writer = new StreamWriter ("retry.txt", false, Encoding.UTF8)) {

				writer.Write (sb.ToString ());
				writer.Close ();
			}
		}
	}
}
